package entity

import (
	"fmt"
	"time"

	"gorm.io/gorm"
)

/*
MariaDB [armatel]> desc terrains2;
+---------------+---------------------+------+-----+---------+----------------+
| Field         | Type                | Null | Key | Default | Extra          |
+---------------+---------------------+------+-----+---------+----------------+
//ID           uint           // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
| id            | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |

//CreatedAt    time.Time      // Automatically managed by GORM for creation time
//UpdatedAt    time.Time      // Automatically managed by GORM for update time
| created_at    | datetime(3)         | YES  |     | NULL    |                |
| updated_at    | datetime(3)         | YES  |     | NULL    |                |
| deleted_at    | datetime(3)         | YES  | MUL | NULL    |                |

| nom           | longtext            | YES  |     | NULL    |                |
| client        | longtext            | YES  |     | NULL    |                |
| etude         | longtext            | YES  |     | NULL    |                |
| objectif      | bigint(20)          | YES  |     | NULL    |                |
| id_logo1      | bigint(20)          | YES  |     | NULL    |                |
| id_logo2      | bigint(20)          | YES  |     | NULL    |                |
| etat          | longtext            | YES  |     | NULL    |                |
| date_creation | datetime(3)         | YES  |     | NULL    |                |
+---------------+---------------------+------+-----+---------+----------------+
*/
type LocalTime time.Time

// # Although the actual type of the data type is time.Time, it does not inherit the built-in method of time.Time, so we need to rewrite part of built-in method, and MarshalJSON is exactly what we need to reimplement.
func (t *LocalTime) MarshalJSON() ([]byte, error) {
	tTime := time.Time(*t)
	return []byte(fmt.Sprintf("\"%v\"", tTime.Format("2006-01-02 15:04:05"))), nil
}

type TerrainEntity struct {
	gorm.Model
	ID       uint   `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	Nom      string `form:"nom" json:"nom" xml:"nom"  binding:"required"`
	Client   string `form:"client" json:"client" xml:"client"`
	Etude    string `form:"etude" json:"etude" xml:"etude"`
	Objectif int    `form:"objectif" binding:"omitempty" json:"objectif" xml:"objectif"`
	Realises int    `form:"realises" binding:"omitempty" json:"realises" xml:"realises"`
	IdLogo1  int    `form:"id_logo1" json:"id_logo1" xml:"id_logo1"`
	IdLogo2  int    `form:"id_logo2" json:"id_logo2" xml:"id_logo2"`
	Etat     string `form:"etat" json:"etat" xml:"etat"`
	//DateCreation time.Time `form:"date_creation" json:"date_creation" xml:"date_creation" gorm:"default:CURRENT_TIMESTAMP()"`
	//DateFin2     datatypes.Date `form:"date_fin" json:"date_fin" xml:"date_fin"`

	// alter table terrains add column date_debut date;
	DateDebut time.Time `form:"date_debut" json:"date_debut" xml:"date_debut" time_format:"2006-01-02" gorm:"default:CURRENT_TIMESTAMP()"`
	DateFin   time.Time `form:"date_fin" json:"date_fin" xml:"date_fin" time_format:"2006-01-02" gorm:"default:CURRENT_TIMESTAMP()"`
	//HeureFin  datatypes.Time `form:"heure_fin" json:"heure_fin" xml:"heure_fin"`

	// permet de consulter les ecoutes de ce terrain sans password ( si derriere un SSO )
	AuthEmail string `form:"auth_email" json:"auth_email" xml:"auth_email"`
	// Cryptpwd: permet de se loguer avec comme user : id terrain
	Cryptpwd string `form:"cryptpwd" json:"cryptpwd" xml:"cryptpwd"`

	Champ1  string `form:"champ1" json:"champ1" xml:"champ1"`
	Champ2  string `form:"champ2" json:"champ2" xml:"champ2"`
	Champ3  string `form:"champ3" json:"champ3" xml:"champ3"`
	Champ4  string `form:"champ4" json:"champ4" xml:"champ4"`
	Champ5  string `form:"champ5" json:"champ5" xml:"champ5"`
	Champ6  string `form:"champ6" json:"champ6" xml:"champ6"`
	Champ7  string `form:"champ7" json:"champ7" xml:"champ7"`
	Champ8  string `form:"champ8" json:"champ8" xml:"champ8"`
	Champ9  string `form:"champ9" json:"champ9" xml:"champ9"`
	Champ10 string `form:"champ10" json:"champ10" xml:"champ10"`
	Champ11 string `form:"champ11" json:"champ11" xml:"champ11"`
	Champ12 string `form:"champ12" json:"champ12" xml:"champ12"`
	Champ13 string `form:"champ13" json:"champ13" xml:"champ13"`
	Champ14 string `form:"champ14" json:"champ14" xml:"champ14"`
	Champ15 string `form:"champ15" json:"champ15" xml:"champ15"`

	InfoChamp1  string `form:"info_champ1" json:"info_champ1" xml:"info_champ1"`
	InfoChamp2  string `form:"info_champ2" json:"info_champ2" xml:"info_champ2"`
	InfoChamp3  string `form:"info_champ3" json:"info_champ3" xml:"info_champ3"`
	InfoChamp4  string `form:"info_champ4" json:"info_champ4" xml:"info_champ4"`
	InfoChamp5  string `form:"info_champ5" json:"info_champ5" xml:"info_champ5"`
	InfoChamp6  string `form:"info_champ6" json:"info_champ6" xml:"info_champ6"`
	InfoChamp7  string `form:"info_champ7" json:"info_champ7" xml:"info_champ7"`
	InfoChamp8  string `form:"info_champ8" json:"info_champ8" xml:"info_champ8"`
	InfoChamp9  string `form:"info_champ9" json:"info_champ9" xml:"info_champ9"`
	InfoChamp10 string `form:"info_champ10" json:"info_champ10" xml:"info_champ10"`
	InfoChamp11 string `form:"info_champ11" json:"info_champ11" xml:"info_champ11"`
	InfoChamp12 string `form:"info_champ12" json:"info_champ12" xml:"info_champ12"`
	InfoChamp13 string `form:"info_champ13" json:"info_champ13" xml:"info_champ13"`
	InfoChamp14 string `form:"info_champ14" json:"info_champ14" xml:"info_champ14"`
	InfoChamp15 string `form:"info_champ15" json:"info_champ15" xml:"info_champ15"`
	//Picture                     []byte    `form:"picture" json:"picture" xml:"picture" gorm:"type:mediumblob"`
	//TestStringIgnoreByMigration string `gorm:"-:all"` // ignore this field when migrate with struct
	Test4 string `form:"test4" json:"test4" xml:"test4" gorm: "type:INTEGER; DEFAULT:0"`
	//DeletedAt time.Time //`gorm.DeletedAt gorm: "type:DATETIME; DEFAULT:NULL"` // Automatically managed by GORM
	//DeletedAt *gorm.DeletedAt `gorm:"index" json:"deleted_at" swaggertype:"primitive,string"`
	//DeletedAt gorm.DeletedAt `gorm:"index"`

	/*
		PRIMARY KEY (`id`),
		KEY `idx_terrains_deleted_at` (`deleted_at`)	*/
}

func (b *TerrainEntity) TableName() string {
	return "terrains"
}

/*MariaDB [armatel]> desc terrains;
+---------------+----------+------+-----+---------------------+----------------+
| Field         | Type     | Null | Key | Default             | Extra          |
+---------------+----------+------+-----+---------------------+----------------+
| id            | int(11)  | NO   | PRI | NULL                | auto_increment |
| nom           | text     | YES  |     | NULL                |                |
| client        | text     | YES  |     | NULL                |                |
| etude         | text     | YES  |     | NULL                |                |
| objectif      | int(11)  | YES  |     | 0                   |                |
| etat          | text     | YES  |     | NULL                |                |
| passwd        | text     | YES  |     | NULL                |                |
| date_creation | datetime | YES  |     | current_timestamp() |                |
| date_debut    | datetime | YES  |     | NULL                |                |
| date_fin      | datetime | YES  |     | NULL                |                |
| logo1         | text     | YES  |     | NULL                |                |
| logo2         | text     | YES  |     | NULL                |                |
| champ1        | text     | YES  |     | NULL                |                |
| champ2        | text     | YES  |     | NULL                |                |
| champ3        | text     | YES  |     | NULL                |                |
| champ4        | text     | YES  |     | NULL                |                |
| champ5        | text     | YES  |     | NULL                |                |
| champ6        | text     | YES  |     | NULL                |                |
| champ7        | text     | YES  |     | NULL                |                |
| champ8        | text     | YES  |     | NULL                |                |
| champ9        | text     | YES  |     | NULL                |                |
| champ10       | text     | YES  |     | NULL                |                |
| champ11       | text     | YES  |     | NULL                |                |
| champ12       | text     | YES  |     | NULL                |                |
| champ13       | text     | YES  |     | NULL                |                |
| champ14       | text     | YES  |     | NULL                |                |
| champ15       | text     | YES  |     | NULL                |                |
| picture       | longblob | YES  |     | NULL                |                |
| cryptpwd      | text     | YES  |     | NULL                |                |
| id_logo1      | int(11)  | YES  |     | NULL                |                |
| id_logo2      | int(11)  | YES  |     | NULL                |                |
+---------------+----------+------+-----+---------------------+----------------+

type Response struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	Error   any    `json:"error,omitempty"`
	Data    any    `json:"data,omitempty"`
	Meta    any    `json:"meta,omitempty"`
}

type ReqListUsers struct {
	Limit  int64  `form:"limit,default=20" binding:"min=1,max=100"`
	Sort   string `form:"sort,default=id" binding:"omitempty,oneof=id createAt lastLoginAt"`
	Order  string `form:"order,default=asc" binding:"omitempty,oneof=asc desc"`
}

func BuildResponseSuccess(message string, data any) Response {
	res := Response{
		Status:  true,
		Message: message,
		Data:    data,
	}
	return res
}

func BuildResponseFailed(message string, err string, data any) Response {
	res := Response{
		Status:  false,
		Message: message,
		Error:   err,
		Data:    data,
	}
	return res
}

GORM use CreatedAt, UpdatedAt to track creating/updating time by convention,
and GORM will set the current time when creating/updating if the fields are defined

type User struct {
  Name string `gorm:"<-:create"` // allow read and create
  Name string `gorm:"<-:update"` // allow read and update
  Name string `gorm:"<-"`        // allow read and write (create and update)
  Name string `gorm:"<-:false"`  // allow read, disable write permission
  Name string `gorm:"->"`        // readonly (disable write permission unless it configured)
  Name string `gorm:"->;<-:create"` // allow read and create
  Name string `gorm:"->:false;<-:create"` // createonly (disabled read from db)
  Name string `gorm:"-"`            // ignore this field when write and read with struct
  Name string `gorm:"-:all"`        // ignore this field when write, read and migrate with struct
  Name string `gorm:"-:migration"`  // ignore this field when migrate with struct
}

type User struct {
  CreatedAt time.Time // Set to current time if it is zero on creating
  UpdatedAt int       // Set to current unix seconds on updating or if it is zero on creating
  Updated   int64 `gorm:"autoUpdateTime:nano"` // Use unix nano seconds as updating time
  Updated   int64 `gorm:"autoUpdateTime:milli"`// Use unix milli seconds as updating time
  Created   int64 `gorm:"autoCreateTime"`      // Use unix seconds as creating time
  Age  *int           `gorm:"default:18"`
  Active sql.NullBool `gorm:"default:true"`
}

type User struct {
  gorm.Model
  Name string
}
// equals
type User struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
  Name string
}
*/
