package entity

import (
	"gorm.io/gorm"
)

type AudioEntity struct {
	gorm.Model
	ID            uint   `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	AudioFilename string `form:"audio_filename" json:"audio_filename" xml:"audio_filename"`
	//IdTerrain     int    `form:"id_terrain" json:"id_terrain" xml:"id_terrain"`
	MediaData []byte `form:"mediadata" json:"mediadata" xml:"mediadata" gorm:"type:longblob" gorm:"-:all"`
}

func (b *AudioEntity) TableName() string {
	return "audios"
}
