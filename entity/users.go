package entity

import (
	"time"

	"gorm.io/gorm"
)

type UserEntity struct {
	gorm.Model
	ID           uint      `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	Username     string    `form:"username" json:"username" xml:"username"  binding:"required"`
	Email        string    `form:"email" json:"email" xml:"email" gorm:"type:varchar(100);uniqueIndex"`
	MobilePhone  string    `form:"mobile_phone" json:"mobile_phone" xml:"mobile_phone" gorm:"type:varchar(100);uniqueIndex"`
	ClearPwd     string    `form:"clearpwd" json:"clearpwd" xml:"clearpwd" gorm:"-:all"`
	CryptPwd     string    `form:"cryptpwd" json:"cryptpwd" xml:"cryptpwd"`
	IsActive     int       `form:"is_active" json:"is_active" xml:"is_active"`
	IsAdmin      int       `form:"is_admin" json:"is_admin" xml:"is_admin"`
	LastLoggedAt time.Time `form:"last_logged_at" json:"last_logged_at" xml:"last_logged_at"`
	//IgnoreMigration string `gorm:"-:migration"` // ignore this field when migrate with struct
	//UpdatedAt time.Time // Automatically managed by GORM for update time
}

func (b *UserEntity) TableName() string {
	return "users"
}

/*
type User struct {
  gorm.Model
  Name string
}
// equals
type User struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
  Name string
}
type User struct {
  ID           uint           // Standard field for the primary key
  Name         string         // A regular string field
  Email        *string        // A pointer to a string, allowing for null values
  Age          uint8          // An unsigned 8-bit integer
  Birthday     *time.Time     // A pointer to time.Time, can be null
  MemberNumber sql.NullString // Uses sql.NullString to handle nullable strings
  ActivatedAt  sql.NullTime   // Uses sql.NullTime for nullable time fields
  CreatedAt    time.Time      // Automatically managed by GORM for creation time
  UpdatedAt    time.Time      // Automatically managed by GORM for update time
}
type Response struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	Error   any    `json:"error,omitempty"`
	Data    any    `json:"data,omitempty"`
	Meta    any    `json:"meta,omitempty"`
}
func BuildResponseSuccess(message string, data any) Response {
	res := Response{
		Status:  true,
		Message: message,
		Data:    data,
	}
	return res
}
func BuildResponseFailed(message string, err string, data any) Response {
	res := Response{
		Status:  false,
		Message: message,
		Error:   err,
		Data:    data,
	}
	return res
}
GORM use CreatedAt, UpdatedAt to track creating/updating time by convention,
and GORM will set the current time when creating/updating if the fields are defined
type User struct {
  Name string `gorm:"<-:create"` // allow read and create
  Name string `gorm:"<-:update"` // allow read and update
  Name string `gorm:"<-"`        // allow read and write (create and update)
  Name string `gorm:"<-:false"`  // allow read, disable write permission
  Name string `gorm:"->"`        // readonly (disable write permission unless it configured)
  Name string `gorm:"->;<-:create"` // allow read and create
  Name string `gorm:"->:false;<-:create"` // createonly (disabled read from db)
  Name string `gorm:"-"`            // ignore this field when write and read with struct
  Name string `gorm:"-:all"`        // ignore this field when write, read and migrate with struct
  Name string `gorm:"-:migration"`  // ignore this field when migrate with struct
}
type User struct {
  CreatedAt time.Time // Set to current time if it is zero on creating
  UpdatedAt int       // Set to current unix seconds on updating or if it is zero on creating
  Updated   int64 `gorm:"autoUpdateTime:nano"` // Use unix nano seconds as updating time
  Updated   int64 `gorm:"autoUpdateTime:milli"`// Use unix milli seconds as updating time
  Created   int64 `gorm:"autoCreateTime"`      // Use unix seconds as creating time
  Age  *int           `gorm:"default:18"`
  Active sql.NullBool `gorm:"default:true"`
}
type User struct {
  gorm.Model
  Name string
}
// equals
type User struct {
  ID        uint           `gorm:"primaryKey"`
  CreatedAt time.Time
  UpdatedAt time.Time
  DeletedAt gorm.DeletedAt `gorm:"index"`
  Name string
}
*/
