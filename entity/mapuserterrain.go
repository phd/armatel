package entity

import (
	"gorm.io/gorm"
)

type UserTerrainEntity struct {
	gorm.Model
	ID        uint `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	IdTerrain int  `form:"id_terrain" json:"id_terrain" xml:"id_terrain" gorm:"uniqueIndex:idx_first_second"`
	IdUser    int  `form:"id_user" json:"id_user" xml:"id_user" gorm:"uniqueIndex:idx_first_second"`
	//CreatedAt    time.Time      // Automatically managed by GORM for creation time
	//UpdatedAt    time.Time      // Automatically managed by GORM for update time
}

func (b *UserTerrainEntity) TableName() string {
	return "mapuserterrain"
}
