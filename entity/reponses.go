package entity

import (
	"gorm.io/gorm"
)

type ReponseEntity struct {
	gorm.Model
	ID            uint   `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	AudioFilename string `form:"audio_filename" json:"audio_filename" xml:"audio_filename"`
	IdTerrain     int    `form:"id_terrain" json:"id_terrain" xml:"id_terrain"`
	Annotation    string `form:"annotation" json:"annotation" xml:"annotation"`
	Sentiment     int    `form:"sentiment" json:"sentiment" xml:"sentiment"`
	Champ1        string `form:"champ1" json:"champ1" xml:"champ1"`
	Champ2        string `form:"champ2" json:"champ2" xml:"champ2"`
	Champ3        string `form:"champ3" json:"champ3" xml:"champ3"`
	Champ4        string `form:"champ4" json:"champ4" xml:"champ4"`
	Champ5        string `form:"champ5" json:"champ5" xml:"champ5"`
	Champ6        string `form:"champ6" json:"champ6" xml:"champ6"`
	Champ7        string `form:"champ7" json:"champ7" xml:"champ7"`
	Champ8        string `form:"champ8" json:"champ8" xml:"champ8"`
	Champ9        string `form:"champ9" json:"champ9" xml:"champ9"`
	Champ10       string `form:"champ10" json:"champ10" xml:"champ10"`
	Champ11       string `form:"champ11" json:"champ11" xml:"champ11"`
	Champ12       string `form:"champ12" json:"champ12" xml:"champ12"`
	Champ13       string `form:"champ13" json:"champ13" xml:"champ13"`
	Champ14       string `form:"champ14" json:"champ14" xml:"champ14"`
	Champ15       string `form:"champ15" json:"champ15" xml:"champ15"`
}

func (b *ReponseEntity) TableName() string {
	return "reponses"
}
