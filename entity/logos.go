package entity

import (
	"gorm.io/gorm"
)

type LogoEntity struct {
	gorm.Model
	ID   uint   `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	Logo []byte `form:"logo" json:"logo" xml:"logo" gorm:"type:longblob" gorm:"-:all"`
}

func (b *LogoEntity) TableName() string {
	return "logos"
}
