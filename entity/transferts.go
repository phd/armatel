package entity

import (
	"database/sql"
	"time"

	"gorm.io/gorm"
)

type TransfertEntity struct {
	gorm.Model
	ID            uint         `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	AudioFilename string       `form:"audio_filename" json:"audio_filename" xml:"audio_filename"`
	SFTPUrl       string       `form:"sftp_url" json:"sftp_url" xml:"sftp_url"`
	FileSize      int          `form:"file_size" json:"file_size" xml:"file_size"`
	DateDebut     time.Time    `form:"date_debut" json:"date_debut" xml:"date_debut" time_format:"2006-01-02"`
	DateFin       time.Time    `form:"date_fin" json:"date_fin" xml:"date_fin" time_format:"2006-01-02"`
	Actif         sql.NullBool `gorm:"default:false"`
	//EnCours       int          `form:"en_cours" json:"en_cours" xml:"en_cours"  gorm:"default:0"`
}

func (b *TransfertEntity) TableName() string {
	return "transferts"
}
