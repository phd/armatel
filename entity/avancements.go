package entity

import (
	"gorm.io/gorm"
)

type AvancementEntity struct {
	gorm.Model
	ID        uint `form:"id" json:"id" xml:"id" gorm:"primary_key" gorm:"AUTO_INCREMENT"` // Standard field for the primary key `gorm:"primary_key" gorm:"AUTO_INCREMENT" json:"id,omitempty"`
	IdTerrain int  `form:"id_terrain" json:"id_terrain" xml:"id_terrain"`
	Realises  int  `form:"realises" json:"realises" xml:"realises"`
}

func (b *AvancementEntity) TableName() string {
	return "avancements"
}
