use armatel;
create table if not exists avancements(id INT primary key AUTO_INCREMENT);
alter table avancements add column(id_terrain INT default NULL);
alter table avancements add column(date_creation datetime default CURRENT_TIMESTAMP);
alter table avancements add column(realises INT default NULL);

