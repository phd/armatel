use armatel;
create table if not exists audios (id INT primary key AUTO_INCREMENT);
alter table audios add column(mediadata LONGBLOB default NULL);
alter table audios add column(audio_filename text default NULL);