-- MySQL dump 10.19  Distrib 10.3.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: armatel
-- ------------------------------------------------------
-- Server version	10.3.38-MariaDB-0ubuntu0.20.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appusers`
--

DROP TABLE IF EXISTS `appusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appusers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` longtext DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `active` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_appusers_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audios`
--

DROP TABLE IF EXISTS `audios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mediadata` longblob DEFAULT NULL,
  `audio_filename` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `media_data` longblob DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_audios_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `avancements`
--

DROP TABLE IF EXISTS `avancements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avancements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_terrain` int(11) DEFAULT NULL,
  `date_creation` datetime DEFAULT current_timestamp(),
  `realises` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ecoutes`
--

DROP TABLE IF EXISTS `ecoutes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecoutes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_terrain` bigint(20) DEFAULT NULL,
  `audio_filename` longtext DEFAULT NULL,
  `date_creation` datetime(3) DEFAULT current_timestamp(3),
  `sentiment` bigint(20) DEFAULT NULL,
  `champ1` longtext DEFAULT NULL,
  `champ2` longtext DEFAULT NULL,
  `champ3` longtext DEFAULT NULL,
  `champ4` longtext DEFAULT NULL,
  `champ5` longtext DEFAULT NULL,
  `champ6` longtext DEFAULT NULL,
  `champ7` longtext DEFAULT NULL,
  `champ8` longtext DEFAULT NULL,
  `champ9` longtext DEFAULT NULL,
  `champ10` longtext DEFAULT NULL,
  `champ11` longtext DEFAULT NULL,
  `champ12` longtext DEFAULT NULL,
  `champ13` longtext DEFAULT NULL,
  `champ14` longtext DEFAULT NULL,
  `champ15` longtext DEFAULT NULL,
  `annotation` longtext DEFAULT NULL,
  `id_audio` bigint(20) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `etat` longtext DEFAULT NULL,
  `passwd` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ecoutes_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logos`
--

DROP TABLE IF EXISTS `logos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` longblob DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terrains`
--

DROP TABLE IF EXISTS `terrains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terrains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` longtext DEFAULT NULL,
  `client` longtext DEFAULT NULL,
  `etude` longtext DEFAULT NULL,
  `objectif` bigint(20) DEFAULT NULL,
  `etat` longtext DEFAULT NULL,
  `passwd` longtext DEFAULT NULL,
  `date_creation` datetime(3) DEFAULT current_timestamp(3),
  `date_fin` date DEFAULT NULL,
  `logo1` text DEFAULT NULL,
  `logo2` text DEFAULT NULL,
  `champ1` longtext DEFAULT NULL,
  `champ2` longtext DEFAULT NULL,
  `champ3` longtext DEFAULT NULL,
  `champ4` longtext DEFAULT NULL,
  `champ5` longtext DEFAULT NULL,
  `champ6` longtext DEFAULT NULL,
  `champ7` longtext DEFAULT NULL,
  `champ8` longtext DEFAULT NULL,
  `champ9` longtext DEFAULT NULL,
  `champ10` longtext DEFAULT NULL,
  `champ11` longtext DEFAULT NULL,
  `champ12` longtext DEFAULT NULL,
  `champ13` longtext DEFAULT NULL,
  `champ14` longtext DEFAULT NULL,
  `champ15` longtext DEFAULT NULL,
  `cryptpwd` longtext DEFAULT NULL,
  `id_logo1` bigint(20) DEFAULT NULL,
  `id_logo2` bigint(20) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `picture` mediumblob DEFAULT NULL,
  `realises` bigint(20) DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_terrains_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=100017 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` longtext DEFAULT NULL,
  `cryptpwd` text DEFAULT NULL,
  `is_admin` bigint(20) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `user_name` longtext DEFAULT NULL,
  `crypt_pwd` longtext DEFAULT NULL,
  `is_active` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_users_email` (`email`),
  KEY `idx_users_deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-20 23:18:19
