use armatel;
create table if not exists users (id INT primary key AUTO_INCREMENT);
alter table users AUTO_INCREMENT=1000;
alter table users add column(username text default NULL);
alter table users add column(cryptpwd text default NULL);
alter table users add column(email text default NULL);
alter table users add column(is_admin int default 0);
