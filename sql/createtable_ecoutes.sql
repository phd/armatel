use armatel;
create table if not exists ecoutes (id INT primary key AUTO_INCREMENT);
alter table ecoutes add column(id_terrain INT default NULL);
alter table ecoutes add column(audio_filename text default NULL);
alter table ecoutes add column(date_creation datetime default CURRENT_TIMESTAMP);
alter table ecoutes add column(sentiment INT default NULL);
alter table ecoutes add column(champ1 text default NULL);
alter table ecoutes add column(champ2 text default NULL);
alter table ecoutes add column(champ3 text default NULL);
alter table ecoutes add column(champ4 text default NULL);
alter table ecoutes add column(champ5 text default NULL);
alter table ecoutes add column(champ6 text default NULL);
alter table ecoutes add column(champ7 text default NULL);
alter table ecoutes add column(champ8 text default NULL);
alter table ecoutes add column(champ9 text default NULL);
alter table ecoutes add column(champ10 text default NULL);
alter table ecoutes add column(champ11 text default NULL);
alter table ecoutes add column(champ12 text default NULL);
alter table ecoutes add column(champ13 text default NULL);
alter table ecoutes add column(champ14 text default NULL);
alter table ecoutes add column(champ15 text default NULL);