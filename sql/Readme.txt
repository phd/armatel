

GORM test:

		   233 Prepare	SELECT column_name, column_default, is_nullable = 'YES', data_type, character_maximum_length, column_type, column_key, extra, column_comment, numeric_precision, numeric_scale , datetime_precision FROM information_schema.columns WHERE table_schema = ? AND table_name = ? ORDER BY ORDINAL_POSITION
		   233 Execute	SELECT column_name, column_default, is_nullable = 'YES', data_type, character_maximum_length, column_type, column_key, extra, column_comment, numeric_precision, numeric_scale , datetime_precision FROM information_schema.columns WHERE table_schema = 'armatel' AND table_name = 'terrains' ORDER BY ORDINAL_POSITION
		   233 Close stmt	
		   233 Query	ALTER TABLE `terrains` ADD `created_at` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` ADD `updated_at` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` ADD `deleted_at` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `nom` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `client` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `etude` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `objectif` bigint
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `id_logo1` bigint
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `id_logo2` bigint
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `etat` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `date_creation` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `date_fin` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `date_debut` datetime(3) NULL
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ1` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ2` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ3` longtext
240516 15:06:07	   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ4` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ5` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ6` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ7` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ8` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ9` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ10` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ11` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ12` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ13` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ14` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `champ15` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `passwd` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `cryptpwd` longtext
		   233 Query	ALTER TABLE `terrains` MODIFY COLUMN `picture` mediumblob
		   233 Query	SELECT DATABASE()
		   233 Prepare	SELECT SCHEMA_NAME from Information_schema.SCHEMATA where SCHEMA_NAME LIKE ? ORDER BY SCHEMA_NAME=? DESC,SCHEMA_NAME limit 1
		   233 Execute	SELECT SCHEMA_NAME from Information_schema.SCHEMATA where SCHEMA_NAME LIKE 'armatel%' ORDER BY SCHEMA_NAME='armatel' DESC,SCHEMA_NAME limit 1
		   233 Close stmt	
		   233 Prepare	SELECT count(*) FROM information_schema.statistics WHERE table_schema = ? AND table_name = ? AND index_name = ?
		   233 Execute	SELECT count(*) FROM information_schema.statistics WHERE table_schema = 'armatel' AND table_name = 'terrains' AND index_name = 'idx_terrains_deleted_at'
		   233 Close stmt	
		   233 Query	CREATE INDEX `idx_terrains_deleted_at` ON `terrains`(`deleted_at`)


type TerrainEntity struct {
	gorm.Model
	Nom                         string    `form:"nom" json:"nom" xml:"nom"  binding:"required"`
	Client                      string    `form:"client" json:"client" xml:"client"`
	Etude                       string    `form:"etude" json:"etude" xml:"etude"`
	Objectif                    int       `form:"objectif" json:"objectif" xml:"objectif"`
	IdLogo1                     int       `form:"id_logo1" json:"id_logo1" xml:"id_logo1"`
	IdLogo2                     int       `form:"id_logo2" json:"id_logo2" xml:"id_logo2"`
	Etat                        string    `form:"etat" json:"etat" xml:"etat"`
	DateFin                     time.Time `form:"date_fin" json:"date_fin" xml:"date_fin"`
	DateDebut                   time.Time `form:"date_debut" json:"date_debut" xml:"date_debut"`
	Champ1                      string    `form:"champ1" json:"champ1" xml:"champ1"`
	Champ2                      string    `form:"champ2" json:"champ2" xml:"champ2"`
	Champ3                      string    `form:"champ3" json:"champ3" xml:"champ3"`
	Champ4                      string    `form:"champ4" json:"champ4" xml:"champ4"`
	Champ5                      string    `form:"champ5" json:"champ5" xml:"champ5"`
	Champ6                      string    `form:"champ6" json:"champ6" xml:"champ6"`
	Champ7                      string    `form:"champ7" json:"champ7" xml:"champ7"`
	Champ8                      string    `form:"champ8" json:"champ8" xml:"champ8"`
	Champ9                      string    `form:"champ9" json:"champ9" xml:"champ9"`
	Champ10                     string    `form:"champ10" json:"champ10" xml:"champ10"`
	Champ11                     string    `form:"champ11" json:"champ11" xml:"champ11"`
	Champ12                     string    `form:"champ12" json:"champ12" xml:"champ12"`
	Champ13                     string    `form:"champ13" json:"champ13" xml:"champ13"`
	Champ14                     string    `form:"champ14" json:"champ14" xml:"champ14"`
	Champ15                     string    `form:"champ15" json:"champ15" xml:"champ15"`
	Passwd                      string    `form:"passwd" json:"passwd" xml:"passwd"`
	Cryptpwd                    string    `form:"cryptpwd" json:"cryptpwd" xml:"cryptpwd"`
	Picture                     []byte    `form:"picture" json:"picture" xml:"picture" gorm:"type:mediumblob"`
	TestStringIgnoreByMigration string    `gorm:"-:migration"` // ignore this field when migrate with struct
}

MariaDB [armatel]> desc terrains;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| id            | int(11)     | NO   | PRI | NULL    | auto_increment |
| nom           | longtext    | YES  |     | NULL    |                |
| client        | longtext    | YES  |     | NULL    |                |
| etude         | longtext    | YES  |     | NULL    |                |
| objectif      | bigint(20)  | YES  |     | NULL    |                |
| etat          | longtext    | YES  |     | NULL    |                |
| passwd        | longtext    | YES  |     | NULL    |                |
| date_creation | datetime(3) | YES  |     | NULL    |                |
| date_debut    | datetime(3) | YES  |     | NULL    |                |
| date_fin      | datetime(3) | YES  |     | NULL    |                |
| logo1         | text        | YES  |     | NULL    |                |
| logo2         | text        | YES  |     | NULL    |                |
| champ1        | longtext    | YES  |     | NULL    |                |
| champ2        | longtext    | YES  |     | NULL    |                |
| champ3        | longtext    | YES  |     | NULL    |                |
| champ4        | longtext    | YES  |     | NULL    |                |
| champ5        | longtext    | YES  |     | NULL    |                |
| champ6        | longtext    | YES  |     | NULL    |                |
| champ7        | longtext    | YES  |     | NULL    |                |
| champ8        | longtext    | YES  |     | NULL    |                |
| champ9        | longtext    | YES  |     | NULL    |                |
| champ10       | longtext    | YES  |     | NULL    |                |
| champ11       | longtext    | YES  |     | NULL    |                |
| champ12       | longtext    | YES  |     | NULL    |                |
| champ13       | longtext    | YES  |     | NULL    |                |
| champ14       | longtext    | YES  |     | NULL    |                |
| champ15       | longtext    | YES  |     | NULL    |                |
| picture       | mediumblob  | YES  |     | NULL    |                |
| cryptpwd      | longtext    | YES  |     | NULL    |                |
| id_logo1      | bigint(20)  | YES  |     | NULL    |                |
| id_logo2      | bigint(20)  | YES  |     | NULL    |                |
| created_at    | datetime(3) | YES  |     | NULL    |                |
| updated_at    | datetime(3) | YES  |     | NULL    |                |
| deleted_at    | datetime(3) | YES  | MUL | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+


MariaDB [armatel]> desc terrains2;
+---------------+---------------------+------+-----+---------+----------------+
| Field         | Type                | Null | Key | Default | Extra          |
+---------------+---------------------+------+-----+---------+----------------+
| id            | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| created_at    | datetime(3)         | YES  |     | NULL    |                |
| updated_at    | datetime(3)         | YES  |     | NULL    |                |
| deleted_at    | datetime(3)         | YES  | MUL | NULL    |                |
| nom           | longtext            | YES  |     | NULL    |                |
| client        | longtext            | YES  |     | NULL    |                |
| etude         | longtext            | YES  |     | NULL    |                |
| objectif      | bigint(20)          | YES  |     | NULL    |                |
| id_logo1      | bigint(20)          | YES  |     | NULL    |                |
| id_logo2      | bigint(20)          | YES  |     | NULL    |                |
| etat          | longtext            | YES  |     | NULL    |                |
| date_creation | datetime(3)         | YES  |     | NULL    |                |
| champ1        | longtext            | YES  |     | NULL    |                |
| champ2        | longtext            | YES  |     | NULL    |                |
| champ3        | longtext            | YES  |     | NULL    |                |
| champ4        | longtext            | YES  |     | NULL    |                |
| champ5        | longtext            | YES  |     | NULL    |                |
| champ6        | longtext            | YES  |     | NULL    |                |
| champ7        | longtext            | YES  |     | NULL    |                |
| champ8        | longtext            | YES  |     | NULL    |                |
| champ9        | longtext            | YES  |     | NULL    |                |
| champ10       | longtext            | YES  |     | NULL    |                |
| champ11       | longtext            | YES  |     | NULL    |                |
| champ12       | longtext            | YES  |     | NULL    |                |
| champ13       | longtext            | YES  |     | NULL    |                |
| champ14       | longtext            | YES  |     | NULL    |                |
| champ15       | longtext            | YES  |     | NULL    |                |
| cryptpwd      | longtext            | YES  |     | NULL    |                |
| picture       | mediumblob          | YES  |     | NULL    |                |
| date_fin      | datetime(3)         | YES  |     | NULL    |                |
| date_debut    | datetime(3)         | YES  |     | NULL    |                |
| passwd        | longtext            | YES  |     | NULL    |                |
+---------------+---------------------+------+-----+---------+----------------+
32 rows in set (0,001 sec)



MariaDB [armatel]> show tables;
+-------------------+
| Tables_in_armatel |
+-------------------+
| appusers          |
| audios            |
| avancements       |
| ecoutes           |
| logos             |
| reponses          |
| terrains          |
| users             |
+-------------------+
8 rows in set (0,000 sec)

MariaDB [armatel]> desc ecoutes;
+----------------+----------+------+-----+---------------------+----------------+
| Field          | Type     | Null | Key | Default             | Extra          |
+----------------+----------+------+-----+---------------------+----------------+
| id             | int(11)  | NO   | PRI | NULL                | auto_increment |
| id_terrain     | int(11)  | YES  |     | NULL                |                |
| audio_filename | text     | YES  |     | NULL                |                |
| date_creation  | datetime | YES  |     | current_timestamp() |                |
| sentiment      | int(11)  | YES  |     | NULL                |                |
| champ1         | text     | YES  |     | NULL                |                |
| champ2         | text     | YES  |     | NULL                |                |
| champ3         | text     | YES  |     | NULL                |                |
| champ4         | text     | YES  |     | NULL                |                |
| champ5         | text     | YES  |     | NULL                |                |
| champ6         | text     | YES  |     | NULL                |                |
| champ7         | text     | YES  |     | NULL                |                |
| champ8         | text     | YES  |     | NULL                |                |
| champ9         | text     | YES  |     | NULL                |                |
| champ10        | text     | YES  |     | NULL                |                |
| champ11        | text     | YES  |     | NULL                |                |
| champ12        | text     | YES  |     | NULL                |                |
| champ13        | text     | YES  |     | NULL                |                |
| champ14        | text     | YES  |     | NULL                |                |
| champ15        | text     | YES  |     | NULL                |                |
| annotation     | text     | YES  |     | NULL                |                |
| id_audio       | int(11)  | YES  |     | NULL                |                |
+----------------+----------+------+-----+---------------------+----------------+
22 rows in set (0,001 sec)



MariaDB [armatel]> desc audios;
+----------------+----------+------+-----+---------+----------------+
| Field          | Type     | Null | Key | Default | Extra          |
+----------------+----------+------+-----+---------+----------------+
| id             | int(11)  | NO   | PRI | NULL    | auto_increment |
| mediadata      | longblob | YES  |     | NULL    |                |
| audio_filename | text     | YES  |     | NULL    |                |
+----------------+----------+------+-----+---------+----------------+


MariaDB [armatel]> desc appusers;
+------------+---------------------+------+-----+---------+----------------+
| Field      | Type                | Null | Key | Default | Extra          |
+------------+---------------------+------+-----+---------+----------------+
| id         | bigint(20) unsigned | NO   | PRI | NULL    | auto_increment |
| created_at | datetime(3)         | YES  |     | NULL    |                |
| updated_at | datetime(3)         | YES  |     | NULL    |                |
| deleted_at | datetime(3)         | YES  | MUL | NULL    |                |
| name       | longtext            | YES  |     | NULL    |                |
| email      | longtext            | YES  |     | NULL    |                |
+------------+---------------------+------+-----+---------+----------------+
6 rows in set (0,001 sec)

MariaDB [armatel]> show create table appusers;
+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Table    | Create Table                                                                                                                                                                                                                                                                                                                                                                                                      |
+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| appusers | CREATE TABLE `appusers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `deleted_at` datetime(3) DEFAULT NULL,
  `name` longtext DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_appusers_deleted_at` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci |
+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0,000 sec)



~ phd ~$ mysql -u admin -p -e "show variables like 'max_connections';"
Enter password: 
+-----------------+-------+
| Variable_name   | Value |
+-----------------+-------+
| max_connections | 151   |
+-----------------+-------+

MariaDB [armatel]> desc avancements;
+---------------+----------+------+-----+---------------------+----------------+
| Field         | Type     | Null | Key | Default             | Extra          |
+---------------+----------+------+-----+---------------------+----------------+
| id            | int(11)  | NO   | PRI | NULL                | auto_increment |
| id_terrain    | int(11)  | YES  |     | NULL                |                |
| date_creation | datetime | YES  |     | current_timestamp() |                |
| realises      | int(11)  | YES  |     | NULL                |                |
+---------------+----------+------+-----+---------------------+----------------+
4 rows in set (0,001 sec)

MariaDB [armatel]> desc logos;
+-------+----------+------+-----+---------+----------------+
| Field | Type     | Null | Key | Default | Extra          |
+-------+----------+------+-----+---------+----------------+
| id    | int(11)  | NO   | PRI | NULL    | auto_increment |
| logo  | longblob | YES  |     | NULL    |                |
+-------+----------+------+-----+---

MariaDB [armatel]> desc audios;
+-----------+----------+------+-----+---------+----------------+
| Field     | Type     | Null | Key | Default | Extra          |
+-----------+----------+------+-----+---------+----------------+
| id        | int(11)  | NO   | PRI | NULL    | auto_increment |
| mediadata | longblob | YES  |     | NULL    |                |
+-----------+----------+------+-----+---------+----------------+
2 rows in set (0,001 sec)
