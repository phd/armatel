use armatel;
create table if not exists enregistrements (id INT primary key AUTO_INCREMENT);
alter table enregistrements add column(id_terrain INT default NULL);
alter table enregistrements add column(date_creation datetime default CURRENT_TIMESTAMP);
alter table enregistrements add column(file_name text default NULL);
alter table enregistrements add column(file_path text default NULL);
alter table enregistrements add column(file_size INT default NULL);
alter table enregistrements add column(file_type text default NULL);
alter table enregistrements add column(ftp_url text default NULL);
