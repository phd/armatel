package helpers

import (
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/securecookie"
)

// Création du cookie sécurisé
var cookieHandler = securecookie.New(securecookie.GenerateRandomKey(64), securecookie.GenerateRandomKey(32))

// Nom du cookies de session
var Admin_CookieSessionName = "admin_session"
var Client_CookieSessionName = "client_session"

func FlashMessage(c *gin.Context, msg string) {
	SetFlashCookie(c, "success", msg)
}

func Admin_SetSession(userName string, c *gin.Context) {
	log.Printf("Admin_SetSession[%v]\n", userName)
	value := map[string]string{
		"name1": userName,
	}

	if encoded, err := cookieHandler.Encode(Admin_CookieSessionName, value); err == nil {
		cookie := &http.Cookie{
			Name:     Admin_CookieSessionName,
			Value:    encoded,
			Path:     "/",
			HttpOnly: true,
			//Secure: true, SEULEMENT DISPONIBLE AVEC HTTPS ACTIVE
		}
		http.SetCookie(c.Writer, cookie)
	}
}

func Admin_GetUserName(c *gin.Context) (username string) {
	log.Printf("Admin_GetUserName...\n")
	if cookie, err := c.Request.Cookie(Admin_CookieSessionName); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode(Admin_CookieSessionName, cookie.Value, &cookieValue); err == nil {
			username = cookieValue["name1"]
			log.Printf("Admin_GetUserName[%v]\n", username)
		}
	}

	return username
}

func Admin_ClearSession(c *gin.Context) {
	log.Printf("Admin_ClearSession...\n")
	cookie := &http.Cookie{
		Name:   Admin_CookieSessionName,
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}

	http.SetCookie(c.Writer, cookie)
}

func Client_SetSession(userName string, c *gin.Context) {
	log.Printf("Client_SetSession... %v\n", userName)
	value := map[string]string{
		"name2": userName,
	}

	if encoded, err := cookieHandler.Encode(Client_CookieSessionName, value); err == nil {
		cookie := &http.Cookie{
			Name:     Client_CookieSessionName,
			Value:    encoded,
			Path:     "/",
			HttpOnly: true,
			//Secure: true, SEULEMENT DISPONIBLE AVEC HTTPS ACTIVE
		}
		http.SetCookie(c.Writer, cookie)
	}
}

func Client_GetUserName(c *gin.Context) (userName string) {

	if cookie, err := c.Request.Cookie(Client_CookieSessionName); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode(Client_CookieSessionName, cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["name2"]
			fmt.Printf("Client_GetUserName cookieValue %v\n", cookieValue)
		} else {
			fmt.Printf("Client_GetUserName error %v\n", err)
		}
	}

	fmt.Printf("Client_GetUserName => %v\n", userName)
	return userName
}

func Client_ClearSession(c *gin.Context) {
	cookie := &http.Cookie{
		Name:   Client_CookieSessionName,
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}

	http.SetCookie(c.Writer, cookie)
}

// Encodage de la valeur du cookie
func encode(value string) string {
	encode := &url.URL{Path: value}
	return encode.String()
}

// Décodage de la valeur du cookie
func decode(value string) string {
	decode, _ := url.QueryUnescape(value)
	return decode
}

func SetFlashCookie(c *gin.Context, name string, value string) {
	cookie := &http.Cookie{
		Name:   name,
		Value:  encode(value),
		Path:   "/",
		MaxAge: 1,
	}

	http.SetCookie(c.Writer, cookie)
}

func GetFlashCookie(c *gin.Context, name string) (value string) {
	cookie, err := c.Request.Cookie(name)

	var cookieValue string
	if err == nil {
		cookieValue = cookie.Value
	} else {
		cookieValue = cookieValue
	}

	return decode(cookieValue)
}
