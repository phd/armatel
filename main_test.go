//Integration Testing with HTTP Client
// main_test.go

package main_test

import (
	"armatel/controllers"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestGetUserByID(t *testing.T) {
	// Setup
	router := gin.Default()
	router.GET("/users/:id", controllers.Admin_LoginForm_GET) //controllers.GetUserByID)

	// Start the server in a separate goroutine
	go func() {
		err := router.Run(":8080")
		assert.NoError(t, err)
	}()

	// Perform the request
	resp, err := http.Get("http://localhost:8080/users/123")
	assert.NoError(t, err)
	defer resp.Body.Close()

	// Assert the response
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	// Additional assertions...
}
