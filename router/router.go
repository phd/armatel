package router

import (
	"armatel/controllers"
	"armatel/db"
	"fmt"
	"os"
	"path"
	"time"

	ratelimit "github.com/JGLTechnologies/gin-rate-limit"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"

	nice "github.com/ekyoung/gin-nice-recovery"
	_ "gorm.io/driver/mysql"
)

func keyFunc(c *gin.Context) string {
	return c.ClientIP()
}

func errorHandler(c *gin.Context, info ratelimit.Info) {
	c.String(429, "Too many requests. Try again in "+time.Until(info.ResetTime).String())
}

func main1111() {
	var err error
	log.Infof("%v starting...", os.Args[0])

	godotenv.Load(".env")
	fmt.Printf("environment = %s \n", os.Getenv("APP_ENV"))

	if os.Getenv("ENVIRONMENT") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	gin.ForceConsoleColor() //gin.DisableConsoleColor()

	r := gin.Default() // Initialize Gin object

	r.Use(nice.Recovery(RecoveryHandler))
	r.MaxMultipartMemory = 8 << 20 // 8 MiB

	//store := cookie.NewStore([]byte(os.Getenv("ARMATEL_SESSION_KEY")))
	//store.Options(sessions.Options{MaxAge: 3600 * 24, HttpOnly: true, Secure: true}) // Set cookie expiration time and secure attributes
	//r.Use(sessions.Sessions("mysession_armatel", store))

	r.Use(LoggerMiddleware())

	/* TODO: Cors Middleware
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://example.com"}
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE"}
	r.Use(cors.New(config))*/

	//setupRouter(r)
	//setupRouter2(r)
	db.SetupDatabase()

	err = r.Run(":8000")
	//err = http.ListenAndServe(":8000",csrf.Protect([]byte("32-byte-long-auth-key"), csrf.Secure(false))(r))
	if err != nil {
		log.Fatalf("gin Run error: %s", err)
	}
}

func RecoveryHandler(c *gin.Context, err interface{}) {
	c.HTML(500, "login/publicpage.html", gin.H{})
}

func SetupRouterPublic(r *gin.Engine) {

	// Page publique sans aucune securite
	r.GET("/", controllers.PublicRootPage_GET)
}

func LoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println(" ==> Request processing")
		start := time.Now()
		// Perform logging before calling the next middleware function
		// Call the next middleware function
		c.Next()
		end := time.Now()
		latency := end.Sub(start)
		// Perform logging after calling the next middleware function

		log.Printf("[%s] %s %s %v", c.Request.Method, c.Request.URL.Path, c.ClientIP(), latency)
		log.Println(" <== Request completed")
	}
}

// Log to file
func LoggerToFile() gin.HandlerFunc {
	logFilePath := "/tmp/"
	logFileName := "armatel.log"
	// log file
	fileName := path.Join(logFilePath, logFileName)
	// write file
	src, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Println("err", err)
	}
	// instantiation
	logger := log.New()
	// Set output
	logger.Out = src
	// Set log level
	logger.SetLevel(log.DebugLevel)
	// Set rotatelogs
	logWriter, err := rotatelogs.New(
		// Split file name
		fileName+".%Y%m%d.log",
		// Generate soft chain, point to the latest log file
		rotatelogs.WithLinkName(fileName),
		// Set maximum save time (7 days)
		rotatelogs.WithMaxAge(7*24*time.Hour),
		// Set log cutting interval (1 day)
		rotatelogs.WithRotationTime(24*time.Hour),
	)
	writeMap := lfshook.WriterMap{
		log.InfoLevel:  logWriter,
		log.FatalLevel: logWriter,
		log.DebugLevel: logWriter,
		log.WarnLevel:  logWriter,
		log.ErrorLevel: logWriter,
		log.PanicLevel: logWriter,
	}
	lfHook := lfshook.NewHook(writeMap, &log.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})
	// Add Hook
	logger.AddHook(lfHook)
	return func(c *gin.Context) {
		// start time
		startTime := time.Now()
		// Processing request
		c.Next()
		// End time
		endTime := time.Now()
		// execution time
		latencyTime := endTime.Sub(startTime)
		// Request mode
		reqMethod := c.Request.Method
		// Request routing
		reqUri := c.Request.RequestURI
		// Status code
		statusCode := c.Writer.Status()
		// Request IP
		clientIP := c.ClientIP()
		// Log format
		logger.WithFields(log.Fields{
			"status_code":  statusCode,
			"latency_time": latencyTime,
			"client_ip":    clientIP,
			"req_method":   reqMethod,
			"req_uri":      reqUri,
		}).Info()
	}
}

/*
return func(c *gin.Context) {
	// start time
	startTime := time.Now()
	// Processing request
	c.Next()
	// End time
	endTime := time.Now()
	// execution time
	latencyTime := endTime.Sub(startTime)
	// Request mode
	reqMethod := c.Request.Method
	// Request routing
	reqUri := c.Request.RequestURI
	// Status code
	statusCode := c.Writer.Status()
	// Request IP
	clientIP := c.ClientIP()
	// Log format
	logger.Infof("| %3d | %13v | %15s | %s | %s |",
		statusCode,
		latencyTime,
		clientIP,
		reqMethod,
		reqUri,
	)
}   */
