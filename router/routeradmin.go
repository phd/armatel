package router

import (
	"armatel/controllers"
	"armatel/helpers"

	"github.com/gin-gonic/gin"
	_ "gorm.io/driver/mysql"
)

func AdminPrivate() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, err := c.Request.Cookie(helpers.Admin_CookieSessionName)

		if err != nil {
			c.Redirect(302, "/admin/login")
			c.Abort() // Très important (sinon pas protégée avec curl -i http://localhost:3000/admin)
		}
	}
}

func SetupRouterAdmin(r *gin.Engine) {
	// Page de connexion Admin
	r.GET("/admin/login", controllers.Admin_LoginForm_GET)
	r.POST("/admin/login", controllers.Admin_Login_POST)
	r.GET("/admin/logout", controllers.Admin_Client_Logout_GET)
	admin := r.Group("/admin") // Nom du groupe "admin" dont l'URI est "/admin"
	admin.Use(AdminPrivate())  // Utilisation du middleware Private()
	{
		// Page admin (privée)
		admin.GET("/test", controllers.Admin_test_Secure_Admin_GET)
		admin.GET("/home", controllers.Admin_Secure_Admin_GET)
		// Autres routes dans "/admin" (privée)
		admin.GET("/accueil", controllers.Admin_Accueil_Get)
		admin.GET("/terrains/", controllers.Terrains_Get)

		// TERRAINS
		admin.GET("/terrains/create", controllers.Terrain_Get)
		admin.GET("/terrains/view/:id", controllers.Terrain_view_Get)
		admin.GET("/terrains/champs/:id", controllers.Terrain_champs_Get)
		admin.GET("/terrains/passwd/:id", controllers.Terrain_view_pwd_Get)

		admin.GET("/terrains/ecoutes/:id", controllers.Terrain_ecoutes_view_Get)

		admin.GET("/terrains/delete/:id", controllers.Terrain_delete_Get)
		admin.POST("/terrains/create", controllers.Terrain_create_Post)
		admin.POST("/terrains/update", controllers.Terrain_update_Post)
		admin.POST("/terrains/updatepwd", controllers.Terrain_updatepwd_Post)

		// import fichier LOGO
		admin.GET("/terrains/uploadlogo/:id", controllers.Terrain_uploadlogo_Get)

		admin.GET("/terrains/uploadlogo2/:id", controllers.Terrain_uploadlogo2_Get)
		admin.GET("/terrains/uploadlogo1/:id", controllers.Terrain_uploadlogo1_Get)

		admin.POST("/terrains/uploadlogo", controllers.Gorm_terrain_uploadlogo_Post)

		admin.POST("/terrains/uploadlogo2", controllers.Gorm_terrain_uploadlogo2_Post)
		admin.POST("/terrains/uploadlogo1", controllers.Gorm_terrain_uploadlogo1_Post)

		admin.POST("/terrains/uploadcsv", controllers.Terrain_uploadcsv_Post)

		//admin.GET("/terrains/viewlogo/:id", controllers.Terrain_viewlogo_Get)
		admin.GET("/terrains/viewlogo1/:id", controllers.Terrain_viewlogo1_Get)
		admin.GET("/terrains/viewlogo2/:id", controllers.Terrain_viewlogo2_Get)

		admin.GET("/terrains/importcsv/", controllers.Terrain_importcsv_Get)

		// ECOUTES
		admin.POST("/ecoutes/create", controllers.Ecoutes_Create_Post)
		admin.POST("/ecoutes/update/:id", controllers.Ecoutes_Create_Post)
		admin.GET("/ecoutes/create", controllers.Ecoutes_Create_Update_Delete_Get)
		admin.GET("/ecoutes/update/:id", controllers.Ecoutes_Create_Update_Delete_Get)
		admin.GET("/ecoutes/delete/:id", controllers.Ecoutes_Create_Update_Delete_Get)

		admin.GET("/ecoutes/", controllers.Ecoutes_Get)

		admin.GET("/audio/:id", controllers.Audio_Get)
		admin.GET("/audios", controllers.AllAudios_Get)
		admin.GET("/audio/inframe/:id", controllers.Admin_AudioInFrame_Get)

		// USERS
		admin.GET("/users/", controllers.All_Users_Get)
		admin.GET("/users/create", controllers.Create_User_Get)
		admin.POST("/users/create", controllers.Create_User_Post)

		admin.GET("/users/update/:id", controllers.Update_User_Get)
		admin.POST("/users/update/:id", controllers.Update_User_Post)

	}
}
