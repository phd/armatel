package router

import (
	"armatel/controllers"
	"armatel/helpers"

	"github.com/gin-gonic/gin"
	_ "gorm.io/driver/mysql"
)

func ClientPrivate() gin.HandlerFunc {
	return func(c *gin.Context) {
		_, err := c.Request.Cookie(helpers.Client_CookieSessionName)
		if err != nil {
			c.Redirect(302, "/client/login")
			c.Abort() // Très important (sinon pas protégée avec curl -i http://localhost:3000/admin)
		}
	}
}

func SetupRouterClient(r *gin.Engine) {
	// Page de connexion Client
	r.GET("/client/login", controllers.Client_LoginForm_GET)
	r.POST("/client/login", controllers.Client_Login_POST)
	r.GET("/client/logout", controllers.Admin_Client_Logout_GET)
	client := r.Group("/client")
	client.Use(ClientPrivate()) // Utilisation du middleware Private()
	{
		// Page admin (privée)
		client.GET("/test", controllers.Client_Secure_Admin_GET)
		client.GET("/home", controllers.Client_Secure_Admin_GET)
		// Autres routes dans "/client"
		client.GET("/view/:id", controllers.Client_Terrain_view_Get)
		client.GET("/accueil", controllers.Client_Accueil_Get)
		client.GET("/terrains/viewlogo2/:id", controllers.Terrain_viewlogo2_Get)
		client.GET("/terrains/viewlogo1/:id", controllers.Terrain_viewlogo1_Get)
		client.GET("/:terrain/audio/inframe/:id", controllers.Client_AudioInFrame_Get)

	}
}
