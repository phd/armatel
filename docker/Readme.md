# Docker & Kubernetes

## Containerization with Docker

### Dockerfile

  FROM golang:1.22 as build

  WORKDIR /app

  COPY go.mod go.sum ./
  RUN go mod download

  COPY . .

  RUN go build -o main .

  FROM gcr.io/distroless/base-debian12

  COPY --from=build /app/main /

  EXPOSE 8080

  CMD ["/main"]

### Start docker

  docker build -t myapp .

  docker run -p 80:8000 myapp

## Orchestration with Kubernetes

### deployment.yaml

  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: myapp
  spec:
    replicas: 3
    selector:
      matchLabels:
        app: myapp
    template:
      metadata:
        labels:
          app: myapp
      spec:
        containers:
          - name: myapp
            image: myapp:latest
            ports:
              - containerPort: 8080

### service.yaml

  apiVersion: v1
  kind: Service
  metadata:
    name: myapp
  spec:
    selector:
      app: myapp
    ports:
      - protocol: TCP
        port: 80
        targetPort: 8000
    type: LoadBalancer

### start POD and service

  kubectl apply -f deployment.yaml
  kubectl apply -f service.yaml