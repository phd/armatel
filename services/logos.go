package services

import (
	"armatel/db"
	"fmt"
	"os"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

func unusedLogoFromDB(db *sqlx.DB, id_terrain string) ([]uint8, error) {
	var err error
	var imgByte []uint8
	err = db.QueryRow("select picture from terrains where id = ?", id_terrain).Scan(&imgByte)
	if err != nil {
		fmt.Printf("terrain_viewlogo_Get QueryRow err:[%v]\n", err)
		return nil, err
	}

	logo_filename := "./tmp/logo_" + uuid.New().String() + ".jpg"
	fmt.Printf("logo_filename OK:[%v]\n", logo_filename)

	f, err := os.Create(logo_filename)
	if err != nil {
		fmt.Printf("os.Create err:[%v]\n", err)
		return nil, err
	}
	defer f.Close()
	fmt.Printf("logo_filename os.Create OK:[%v]\n", logo_filename)

	err = os.WriteFile(logo_filename, imgByte, 0644)
	if err != nil {
		fmt.Printf("os.WriteFile err:[%v]\n", err)
		return nil, err
	}
	fmt.Printf("os.WriteFile OK:[%v]\n", logo_filename)
	/*
		var img image.Image
		img, _, err = image.Decode(bytes.NewReader(imgByte))
		if err != nil {
			fmt.Printf("image.Decode err:[%v]\n", err)
			return
		}

		var opts jpeg.Options
		opts.Quality = 1
		if err = jpeg.Encode(f, img, &opts); err != nil {
			log.Printf("failed to encode: %v\n", err)
			return
		}
	*/
	/*
		var images []string
		var img []byte
		for i := 0; i < 25; i++ {
			img, err = qrcode.Encode("https://example.org", qrcode.Medium, 256)
			img2 := base64.StdEncoding.EncodeToString(img)
			images = append(images, img2)
			if err != nil {
				fmt.Print(err)
			}
		}*/
	return imgByte, nil
}

func LogoFromBlob(id_logo string) ([]uint8, error) {
	var imgByte []uint8
	if db, err := db.GetInstance(); err == nil {
		err := db.QueryRow("select logo from logos where id = ?", id_logo).Scan(&imgByte)
		if err != nil {
			fmt.Printf("LogoFromBlob err:[%v]\n", err)
			return nil, err
		}

		if false {

			tmp_fname := fmt.Sprintf("/tmp/logo_%s_%s.jpg", id_logo, uuid.New().String())
			fmt.Printf("logo_filename OK:[%v]\n", tmp_fname)

			f, err := os.Create(tmp_fname)
			if err != nil {
				fmt.Printf("os.Create err:[%v]\n", err)
				return nil, err
			}
			defer f.Close()
			fmt.Printf("logo_filename os.Create OK:[%v]\n", tmp_fname)

			err = os.WriteFile(tmp_fname, imgByte, 0644)
			if err != nil {
				fmt.Printf("os.WriteFile err:[%v]\n", err)
				return nil, err
			}
			fmt.Printf("os.WriteFile OK:[%v]\n", tmp_fname)
			/*
				var img image.Image
				img, _, err = image.Decode(bytes.NewReader(imgByte))
				if err != nil {
					fmt.Printf("image.Decode err:[%v]\n", err)
					return
				}

				var opts jpeg.Options
				opts.Quality = 1
				if err = jpeg.Encode(f, img, &opts); err != nil {
					log.Printf("failed to encode: %v\n", err)
					return
				}
			*/
			/*
				var images []string
				var img []byte
				for i := 0; i < 25; i++ {
					img, err = qrcode.Encode("https://example.org", qrcode.Medium, 256)
					img2 := base64.StdEncoding.EncodeToString(img)
					images = append(images, img2)
					if err != nil {
						fmt.Print(err)
					}
				}*/
		}
	}
	return imgByte, nil
}

func ImgFromBlob(id_terrain string, columnName string) ([]uint8, error) {
	var imgByte []uint8
	if db, err := db.GetInstance(); err == nil {
		var err error

		err = db.QueryRow("select "+columnName+" from terrains where id = ?", id_terrain).Scan(&imgByte)
		if err != nil {
			fmt.Printf("terrain_viewlogo_Get QueryRow %v err:[%v]\n", columnName, err)
			return nil, err
		}

		if true {

			tmp_fname := fmt.Sprintf("./tmp/img_%s_%s_%s.jpg", columnName, id_terrain, uuid.New().String())
			fmt.Printf("logo_filename OK:[%v]\n", tmp_fname)

			f, err := os.Create(tmp_fname)
			if err != nil {
				fmt.Printf("os.Create err:[%v]\n", err)
				return nil, err
			}
			defer f.Close()
			fmt.Printf("logo_filename os.Create OK:[%v]\n", tmp_fname)

			err = os.WriteFile(tmp_fname, imgByte, 0644)
			if err != nil {
				fmt.Printf("os.WriteFile err:[%v]\n", err)
				return nil, err
			}
			fmt.Printf("os.WriteFile OK:[%v]\n", tmp_fname)
			/*
				var img image.Image
				img, _, err = image.Decode(bytes.NewReader(imgByte))
				if err != nil {
					fmt.Printf("image.Decode err:[%v]\n", err)
					return
				}

				var opts jpeg.Options
				opts.Quality = 1
				if err = jpeg.Encode(f, img, &opts); err != nil {
					log.Printf("failed to encode: %v\n", err)
					return
				}
			*/
			/*
				var images []string
				var img []byte
				for i := 0; i < 25; i++ {
					img, err = qrcode.Encode("https://example.org", qrcode.Medium, 256)
					img2 := base64.StdEncoding.EncodeToString(img)
					images = append(images, img2)
					if err != nil {
						fmt.Print(err)
					}
				}*/
		}
	}
	return imgByte, nil
}
