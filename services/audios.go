package services

import (
	"armatel/db"
	my_db "armatel/db"
	"armatel/entity"
	"encoding/base64"
	"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
)

func GetOneAudio(id string) (entity.AudioEntity, string, error) {

	var audio entity.AudioEntity

	mydb, err := my_db.GetInstance2()
	if err == nil {
		fmt.Printf("GetOneAudio %v\n", 1)
		/*	// Get the first record ordered by primary key
			// SELECT * FROM users ORDER BY id LIMIT 1;
			result := db.First(&user)
			result.RowsAffected // returns count of records found
			result.Error        // returns error or nil

			db.First(&user, 10)
			// SELECT * FROM users WHERE id = 10;
		*/
		result := mydb.First(&audio, id)
		if result.Error != nil {
			//fmt.Printf("found %v audio: %v\n", result.RowsAffected, audio)
			fmt.Printf("NOK found %v audio: %v\n", result.RowsAffected, audio.ID)
			return audio, "", result.Error
		} else {

			fmt.Printf("OK found %v audio: %v\n", result.RowsAffected, audio.ID)
			if db, err := my_db.GetInstance(); err == nil {
				var audioBytes []uint8
				audioBytes, err = AudioFromBlob(db, strconv.Itoa(int(audio.ID)))
				if err != nil {
					fmt.Printf("Error retrieving audio '%d' error %v\n", audio.ID, err)
				} else {
					fmt.Printf("OK retrieving audio '%d' len %v\n", audio.ID, len(audioBytes))
				}
				//form_params["audioBytes"] = base64.StdEncoding.EncodeToString(audioBytes)

				return audio, base64.StdEncoding.EncodeToString(audioBytes), result.Error
			}
		}
	}
	return audio, "", err
}

func GetAudiosOfTerrain(id_terrain string) ([]entity.AudioEntity, int64, error) {
	var audios []entity.AudioEntity

	fmt.Printf("GetAudiosOfTerrain %v\n", id_terrain)
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Where("id_terrain = ?", id_terrain).Find(&audios)
		//// SELECT * FROM ecoutes WHERE id_terrain = 'jinzhu'
		//db.Where("name = ? AND age >= ?", "jinzhu", "22").Find(&users)
		fmt.Printf("\nfound audios: %v\n", audios)
		return audios, result.RowsAffected, nil
	}
	return nil, 0, err
}

func GetAllAudios() ([]entity.AudioEntity, int64, error) {
	fmt.Printf("GetAllAudios\n")
	var audios []entity.AudioEntity

	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Find(&audios)
		fmt.Printf("found %v audios\n", len(audios))
		return audios, result.RowsAffected, nil
	}
	return nil, 0, err
}

func AudioFromBlob(db *sqlx.DB, id_audio string) ([]uint8, error) {
	fmt.Printf("AudioFromBlob:[%v]\n", id_audio)
	var err error
	var audioByte []uint8
	columnName := "mediadata"
	err = db.QueryRow("select mediadata from audios where id = ?", id_audio).Scan(&audioByte)
	if err != nil {
		fmt.Printf("AudioFromBlob QueryRow %v err:[%v]\n", columnName, err)
		return nil, err
	}
	fmt.Printf("OK AudioFromBlob:[%v] len %v \n", id_audio, len(audioByte))
	return audioByte, nil
}
