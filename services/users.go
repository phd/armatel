package services

import (
	"armatel/db"
	"armatel/entity"
	"fmt"
	"log"
	"strconv"
	"time"
)

func GetAllUsers() ([]entity.UserEntity, int64, error) {
	var users []entity.UserEntity
	/*
		// Get all records
		result := db.Find(&users)
		// SELECT * FROM users;
		result.RowsAffected // returns found records count, equals `len(users)`
		result.Error        // returns error
	*/
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Find(&users)
		return users, result.RowsAffected, nil

		/*type APIUser struct {
			ID       uint
			Username string
		}
		mydb.Model(&entity.UserEntity{}).Limit(10).Find(&APIUser{})*/

		//mydb.Debug().Table("users").Select("users.name, emails.email").Joins("left join mapuserterrain on mapuserterrain.user_id = users.id").Scan(&results)

		//result := mydb.Joins("mapuserterrain").Find(&users)
	}
	return nil, 0, err
}

func GetAllUsersWithTerrain() ([]map[string]string, error) {
	var arr2 []map[string]string
	mydb, err := db.GetInstance2()
	if err == nil {
		var users []entity.UserEntity
		mydb.Find(&users)
		//return users, result.RowsAffected, nil
		for _, uu := range users {
			form_params := map[string]string{"foo": "1", "bar": "2"}
			form_params["ID"] = strconv.Itoa(int(uu.ID))
			form_params["Username"] = uu.Username
			form_params["Email"] = uu.Email
			form_params["MobilePhone"] = uu.MobilePhone
			form_params["CreatedAt"] = uu.CreatedAt.Format(time.DateTime)
			form_params["UpdatedAt"] = uu.UpdatedAt.Format(time.DateTime)
			form_params["LastLoggedAt"] = uu.LastLoggedAt.Format(time.DateTime)
			form_params["Email"] = uu.Email
			var mmap entity.UserTerrainEntity
			mydb.Where("id_user = ?", uu.ID).First(&mmap)
			form_params["IdTerrain"] = strconv.Itoa(mmap.IdTerrain)
			arr2 = append(arr2, form_params)
		}
	}
	return arr2, err
}

func GetOneUser(id string) (entity.UserEntity, error) {
	var user entity.UserEntity
	/*
		// Get all records
		result := db.Find(&users)
		// SELECT * FROM users;
		result.RowsAffected // returns found records count, equals `len(users)`
		result.Error        // returns error
	*/
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Find(&user, id)
		return user, result.Error
	}
	return user, err
}

func GetOneUserByName(username string) (entity.UserEntity, error) {
	var user entity.UserEntity
	/*
		// Get all records
		result := db.Find(&users)
		// SELECT * FROM users;
		result.RowsAffected // returns found records count, equals `len(users)`
		result.Error        // returns error
	*/
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Where("username = ?", username).First(&user)
		//result := mydb.Find(&user, id)
		return user, result.Error
	} else {
		log.Printf("GetOneUserByName %v error: %v\n", username, err)
	}
	return user, err
}

func CreateOneUser(user entity.UserEntity) error {
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Create(&user)
		return result.Error
	}
	return err
}

func SaveOneUser(user entity.UserEntity) error {
	mydb, err := db.GetInstance2()
	if err == nil {
		/* db.First(&user)
		user.Name = "jinzhu 2"
		user.Age = 100
		db.Save(&user)
		// UPDATE users SET name='jinzhu 2', age=100, birthday='2016-01-01', updated_at = '2013-11-17 21:34:10' WHERE id=111;
		*/
		result := mydb.Save(&user)
		if result.Error == nil {
			fmt.Printf("OK saved %v user: %v\n", result.RowsAffected, user)
			return nil
		} else {
			return result.Error
		}
	}
	return nil
}

/*

type User struct {
  gorm.Model
  Name string
  Age  *int           `gorm:"default:18"`
  Active sql.NullBool `gorm:"default:true"`
}

type User struct {
  ID        string `gorm:"default:uuid_generate_v3()"` // db func
  FirstName string
  LastName  string
  Age       uint8
  FullName  string `gorm:"->;type:GENERATED ALWAYS AS (concat(firstname,' ',lastname));default:(-);"`
}

type User struct {
  CreatedAt time.Time // Set to current time if it is zero on creating
  UpdatedAt int       // Set to current unix seconds on updating or if it is zero on creating
  Updated   int64 `gorm:"autoUpdateTime:nano"` // Use unix nano seconds as updating time
  Updated   int64 `gorm:"autoUpdateTime:milli"`// Use unix milli seconds as updating time
  Created   int64 `gorm:"autoCreateTime"`      // Use unix seconds as creating time
}

db.Debug().Table("users").Select("users.name, emails.email").Joins("left join emails on emails.user_id = users.id").Scan(&results)

db.First(&user) // an error is raised it the table is empty
db.Limit(1).Find(&user) // no error

import (
    "fmt"
    "gorm.io/driver/mysql"
    "gorm.io/gorm"
    "gorm.io/gorm/logger"
)

type BookData struct {
    Id                string              `gorm:"primary_key;column:book_id"`
    AuthorId          string              `gorm:"column:author_id"`
    Author            AuthorData          `gorm:"ForeignKey:Id;References:AuthorId"`
    PublisherProperty []PublisherProperty `gorm:"ForeignKey:Id"`
}

type AuthorData struct {
    Id   string `gorm:"primary_key;column:author_id"`
    Name string `gorm:"column:author_name"`
}

type PublisherProperty struct {
    Id           string `gorm:"primary_key;column:book_id"`
    PublisherId  string `gorm:"primary_key;column:publisher_id"`
    PublisherTxt string `gorm:"column:publisher_txt"`
}

var db *gorm.DB

func init() {
    dsn := "user:pass@tcp(127.0.0.1:3306)/temp?charset=utf8mb4&parseTime=True&loc=Local"
    var err error
    db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
        Logger: logger.Default.LogMode(logger.Info),
    })
    if err != nil {
        panic(err)
    }
}

func main() {
    var books []BookData
    err := db.Preload("Author").Preload("PublisherProperty").
        Find(&books).Limit(10).Error
    if err != nil {
        fmt.Println(err)
        return
    }
    fmt.Println(books)
    // output:
    // [{1 1 {1 author1} [{1 1 } {1 2 }]} {2 1 {1 author1} []}]
}
*/
