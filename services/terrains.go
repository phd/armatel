package services

import (
	"armatel/db"
	"armatel/entity"
	"fmt"
)

func CreateOneTerrain(terr entity.TerrainEntity) (uint, int64, error) {
	//user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Create(&terr) // pass pointer of data to Create

		//terr.ID             // returns inserted data's primary key
		//result.Error        // returns error
		//result.RowsAffected // returns inserted records count
		if result.Error == nil {
			return terr.ID, result.RowsAffected, result.Error
		} else {
			return 0, 0, result.Error
		}
	}
	return 0, 0, err
}

func GetAllTerrains() ([]entity.TerrainEntity, int64, error) {
	fmt.Printf("GetAllTerrains\n")
	var terrains []entity.TerrainEntity
	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Find(&terrains)
		fmt.Printf("found terrains %v\n", len(terrains))
		return terrains, result.RowsAffected, nil
	}
	return nil, 0, err
}

func SaveOneTerrain(terrain entity.TerrainEntity) error {
	mydb, err := db.GetInstance2()
	if err == nil {
		/* db.First(&user)
		user.Name = "jinzhu 2"
		user.Age = 100
		db.Save(&user)
		// UPDATE users SET name='jinzhu 2', age=100, birthday='2016-01-01', updated_at = '2013-11-17 21:34:10' WHERE id=111;
		*/
		result := mydb.Save(&terrain)
		if result.Error == nil {
			fmt.Printf("saved %v terrain: %v\n", result.RowsAffected, terrain)
			return nil
		} else {
			return result.Error
		}
	}
	return nil
}

func GetOneTerrain(id string) (entity.TerrainEntity, error) {
	fmt.Printf("GetOneTerrain %v\n", id)
	var terrain entity.TerrainEntity

	mydb, err := db.GetInstance2()
	if err == nil {
		/*	// Get the first record ordered by primary key
			// SELECT * FROM users ORDER BY id LIMIT 1;
			result := db.First(&user)
			result.RowsAffected // returns count of records found
			result.Error        // returns error or nil

			db.First(&user, 10)
			// SELECT * FROM users WHERE id = 10;
		*/
		result := mydb.First(&terrain, id)
		if result.Error == nil {
			fmt.Printf("found %v terrain: %v\n", result.RowsAffected, terrain)
			return terrain, nil
		} else {
			return terrain, result.Error
		}
	}
	return terrain, err
}
