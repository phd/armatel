package services

import (
	"armatel/db"
	"armatel/entity"
	"fmt"
)

func GetEcoutesOfTerrain(id_terrain string) ([]entity.EcouteEntity, int64, error) {
	var ecoutes []entity.EcouteEntity

	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Where("id_terrain = ?", id_terrain).Find(&ecoutes)
		//// SELECT * FROM ecoutes WHERE id_terrain = 'jinzhu'
		//db.Where("name = ? AND age >= ?", "jinzhu", "22").Find(&users)
		fmt.Printf("\nfound ecoutes: %v\n", ecoutes)
		return ecoutes, result.RowsAffected, nil
	}
	return nil, 0, err
}

func GetAllEcoutes() ([]entity.EcouteEntity, int64, error) {
	fmt.Printf("GetAllTerrains %v\n", 1)
	var ecoute []entity.EcouteEntity

	mydb, err := db.GetInstance2()
	if err == nil {
		result := mydb.Find(&ecoute)
		fmt.Printf("found terrains %v\n", ecoute)
		return ecoute, result.RowsAffected, nil
	}
	return nil, 0, err
}

func SaveOneEcoutes(terrain entity.EcouteEntity) error {
	mydb, err := db.GetInstance2()
	if err == nil {
		/* db.First(&user)
		user.Name = "jinzhu 2"
		user.Age = 100
		db.Save(&user)
		// UPDATE users SET name='jinzhu 2', age=100, birthday='2016-01-01', updated_at = '2013-11-17 21:34:10' WHERE id=111;
		*/
		result := mydb.Save(&terrain)
		if result.Error == nil {
			fmt.Printf("saved %v ecoute: %v\n", result.RowsAffected, terrain)
			return nil
		} else {
			return result.Error
		}
	}
	return nil
}

func DeleteOneEcoute(ecoute entity.EcouteEntity) error {
	fmt.Printf("DeleteOneEcoute %v\n", ecoute.ID)
	mydb, err := db.GetInstance2()
	if err == nil {
		// Email's ID is `10`
		//db.Delete(&email)
		// DELETE from emails where id = 10;
		//You can delete matched records permanently with Unscoped
		//db.Unscoped().Delete(&order)
		// DELETE FROM orders WHERE id=10;
		result := mydb.Unscoped().Delete(&ecoute)
		return result.Error
	}
	return err
}

func GetOneEcoute(id string) (entity.EcouteEntity, error) {
	fmt.Printf("GetOneTerrain %v\n", 1)
	var ecoute entity.EcouteEntity

	mydb, err := db.GetInstance2()
	if err == nil {
		/*	// Get the first record ordered by primary key
			// SELECT * FROM users ORDER BY id LIMIT 1;
			result := db.First(&user)
			result.RowsAffected // returns count of records found
			result.Error        // returns error or nil

			db.First(&user, 10)
			// SELECT * FROM users WHERE id = 10;
		*/
		result := mydb.First(&ecoute, id)
		if result.Error == nil {
			fmt.Printf("found %v terrain: %v\n", result.RowsAffected, ecoute)
			return ecoute, nil
		} else {
			return ecoute, result.Error
		}
	}
	return ecoute, err
}
