package services

import (
	"armatel/db"
	"armatel/entity"
	"log"
)

func CreateOneUserTerrainMap(mmap entity.UserTerrainEntity) (uint, int64, error) {
	mydb, err := db.GetInstance2()
	if err == nil { /*
			var map_user_terr models.UserTerrainEntity
			map_user_terr.IdTerrain = id_terrain
			map_user_terr.IdUser = id_user_nod
		*/
		var uu entity.UserTerrainEntity
		err2 := mydb.Where("id_terrain = ? AND `id_user` = ? ", mmap.IdTerrain, mmap.IdUser).First(&uu)
		if err2 != nil {
			result := mydb.Create(&mmap) // pass pointer of data to Create
			//terr.ID             // returns inserted data's primary key
			//result.Error        // returns error
			//result.RowsAffected // returns inserted records count
			if result.Error == nil {
				log.Printf("OK mydb.Create=%v\n", mmap.ID)
				return mmap.ID, result.RowsAffected, result.Error
			} else {
				log.Printf("NOK mydb.Create=%v\n", result.Error)
				return 0, 0, result.Error
			}
		} else {
			log.Printf("GORM mydb.Where err %v\n", err2)
			result := mydb.Save(&mmap)
			if result.Error == nil {
				log.Printf("OK mydb.Save=%v\n", mmap.ID)
				return mmap.ID, result.RowsAffected, result.Error
			} else {
				log.Printf("NOK mydb.Save=%v\n", result.Error)
				return 0, 0, result.Error
			}
		}
	}
	return 0, 0, err
}

/*
func CreateOrUpdate(db *gorm.DB, model interface{}, where interface{}, update interface{}) (interface{}, error) {
    var result interface{}
    err := db.Model(model).Where(where).First(result).Error
    if err != nil {
        if !errors.Is(err, gorm.ErrRecordNotFound) {
            return nil, err
        } else {
            //insert
            if err = db.Model(model).Create(update).Error; err != nil {
                return nil, err
            }
        }
    }
    //not update some field
    reflect.ValueOf(update).Elem().FieldByName("someField").SetInt(0)
    if err = db.Model(model).Where(where).Updates(update).Error; err != nil {
        return nil, err
    }
    return update, nil
}

*/
