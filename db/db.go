package db

/*
SQL Injection
SQL Injection is a vulnerability that allows attackers to execute arbitrary SQL commands by inserting malicious input into SQL queries. To prevent SQL Injection vulnerabilities in Gin applications, you can:

– Use parameterized queries or prepared statements to separate SQL code from user input.
– Avoid constructing SQL queries using string concatenation or interpolation.
– Use an ORM or query builder that automatically handles parameterization and sanitization of user input.
*/
import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	//"github.com/jinzhu/gorm"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"armatel/entity"
)

// global variable: access it using db.GetInstance()
var armatel_db *sqlx.DB
var armatel_db2 *gorm.DB

type DatabaseSession struct {
	db *sql.DB
}

var once sync.Once
var session *DatabaseSession

// TECHNO *sql.DB
func GetDatabaseSession() *DatabaseSession {
	once.Do(func() {
		db, err := sql.Open("mysql", "user:password@tcp(localhost:3306)/database")
		if err != nil {
			log.Fatal(err)
		}
		// Configure database session settings
		db.SetMaxOpenConns(100)
		db.SetMaxIdleConns(10)
		session = &DatabaseSession{db: db}
	})
	return session
}

/*
// La structure de données
type Users1 struct {
	Id       int    `gorm:"AUTO_INCREMENT" form:"id"`
	Name     string `gorm:"not null;unique" form:"username"` // Utilisateur unique!
	Password string `gorm:"not null" form:"password"`
}

// TECHNO SQLite

func InitDb() *gorm.DB { // Ouverture de la connexion vers la BDD SQLite
	db, err := gorm.Open("sqlite3", "./data.db")
	// Afficher les requêtes SQL (facultatif)
	db.LogMode(true)

	// Création de la table "users"
	if !db.HasTable(&Users1{}) {
		db.CreateTable(&Users1{})
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&Users1{})
	}
	if err != nil {
		panic(err)
	}
	return db

type Product struct {
  ID    uint `gorm:"primaryKey;default:auto_random()"`
  Code  string
  Price uint
}

func main() {
  db, err := gorm.Open(mysql.Open("root:@tcp(127.0.0.1:4000)/test"), &gorm.Config{})
  if err != nil {
    panic("failed to connect database")
  }

  db.AutoMigrate(&Product{})

  sqlDB, err := db.DB()

// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
sqlDB.SetMaxIdleConns(10)

// SetMaxOpenConns sets the maximum number of open connections to the database.
sqlDB.SetMaxOpenConns(100)

// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
sqlDB.SetConnMaxLifetime(time.Hour)

  insertProduct := &Product{Code: "D42", Price: 100}

  db.Create(insertProduct)
  fmt.Printf("insert ID: %d, Code: %s, Price: %d\n",
    insertProduct.ID, insertProduct.Code, insertProduct.Price)

  readProduct := &Product{}
  db.First(&readProduct, "code = ?", "D42") // find product with code D42

  fmt.Printf("read ID: %d, Code: %s, Price: %d\n",
    readProduct.ID, readProduct.Code, readProduct.Price)
}
*/

func GetInstance2() (*gorm.DB, error) {
	if armatel_db2 == nil {
		return nil, fmt.Errorf("armatel_db2 is NIL")
	}
	dbase, err := armatel_db2.DB()
	if err != nil {
		return nil, err
	}
	err = dbase.Ping()
	if err != nil {
		return nil, err
	}

	return armatel_db2, nil
}

func GetInstance() (*sqlx.DB, error) {
	var err error
	if armatel_db == nil {
		return nil, fmt.Errorf("armatel_db is NIL")
	}
	err = armatel_db.Ping()
	if err != nil {
		fmt.Printf("error pinging sqlx db: %v\n", err)
		err = SetupDatabase()
	}
	return armatel_db, err
}

func SetupDatabase() error {
	var err error
	armatel_db, err = sqlx.Connect("mysql", os.Getenv("DB_URL"))
	if err != nil {
		fmt.Printf("error opening sqlx db: %v\n", err)
		return err
	}

	armatel_db2, err = gorm.Open(mysql.Open(os.Getenv("DB_URL")), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	//armatel_db2.LogMode(true) // Afficher les requêtes SQL (facultatif)
	if err != nil {
		fmt.Printf("error gorm.Open db: %v\n", err)
		return err
	} else {
		var err error
		dbase, err := armatel_db2.DB()
		if err != nil {
			panic(err)
		}
		err = dbase.Ping()
		if err != nil {
			panic(err)
		}

		err = armatel_db2.AutoMigrate(&entity.TerrainEntity{})
		if err != nil {
			panic(err)
		} else {
			var terr entity.TerrainEntity
			terr.ID = 99999
			terr.DeletedAt = gorm.DeletedAt{Time: time.Now(), Valid: true}

			result := armatel_db2.Create(&terr) // pass pointer of data to Create

			//terr.ID             // returns inserted data's primary key
			//result.Error        // returns error
			//result.RowsAffected // returns inserted records count
			if result.Error == nil {
				log.Printf("OK gorm.Create db: %v\n", terr.ID)
			} else {
				//error gorm.Create db: Error 1062 (23000): Duplicate entry '100000' for key 'PRIMARY'
				log.Printf("error gorm.Create db: %v\n", result.Error)
			}
		}

		err = armatel_db2.AutoMigrate(&entity.EcouteEntity{})
		if err != nil {
			panic(err)
		}
		err = armatel_db2.AutoMigrate(&entity.AudioEntity{})
		if err != nil {
			panic(err)
		}
		err = armatel_db2.AutoMigrate(&entity.LogoEntity{})
		if err != nil {
			panic(err)
		}
		err = armatel_db2.AutoMigrate(&entity.UserEntity{})
		if err != nil {
			panic(err)
		}
		err = armatel_db2.AutoMigrate(&entity.UserTerrainEntity{})
		if err != nil {
			panic(err)
		}
	}
	return nil
}
