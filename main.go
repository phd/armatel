package main

import (
	"armatel/db"
	"armatel/router"
	"embed"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/alecthomas/kingpin"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"

	nice "github.com/ekyoung/gin-nice-recovery"
	_ "gorm.io/driver/mysql"
)

//go:embed templates
var tmplEmbed embed.FS

//go:embed static
var staticEmbedFS embed.FS

type staticFS struct {
	fs fs.FS
}

func (sfs *staticFS) Open(name string) (fs.File, error) {
	return sfs.fs.Open(filepath.Join("static", name))
}

var staticEmbed = &staticFS{staticEmbedFS}

func main() {
	fmt.Printf("%v starting with %v args ...", os.Args[0], len(os.Args)-1)
	var (
		// prometheus controller own params
		param_log_level = kingpin.Flag("log-level", "trace,debug,info,warn,error").Default("debug").String()
		param_log_path  = kingpin.Flag("log-path", "output log file path").Default("armatel.log").String()
		/*param_metrics_port        = kingpin.Flag("metrics-port", "Local port to listen on for web interface and telemetry.").Default("8081").String()
		param_metrics_path        = kingpin.Flag("metrics-path", "Path under which to expose metrics.").Default("/metrics").String()
		param_freeswitch_port     = kingpin.Flag("freeswitch-port", "freeswitch target component port.").Default("8021").String()
		param_freeswitch_ip       = kingpin.Flag("freeswitch-ip", "freeswitch target component ip address.").Default("127.0.0.1").String()
		param_freeswitch_pwd      = kingpin.Flag("freeswitch-password", "freeswitch ESL interface password.").Default("ClueCon").String()
		param_fs_dialpan_variable = kingpin.Flag("freeswitch-variable", "freeswitch dialplan variable set with oauth token value.").Default("mytoken").String()
		param_oauth_url           = kingpin.Flag("oauth-url", "OAuth server url.").Default("https://auth.staging.libon.com/oauth/token").String()
		param_oauth_max_attempts  = kingpin.Flag("oauth-max-attempts", "OAuth max attempts to request a token.").Default("5").String()
		param_oauth_retry_delay   = kingpin.Flag("oauth-retry-delay", "OAuth delay between each attempt (second).").Default("60").String()
		param_oauth_token_scopes  = kingpin.Flag("oauth-token-scopes", "OAuth token requested scope(s).").Default("C4_USER_READ").String()*/
	)

	kingpin.Parse() // -> read parameters given on command line

	if *param_log_level == "info" {
		log.SetLevel(log.InfoLevel) // Something noteworthy happened
	} else if *param_log_level == "error" {
		log.SetLevel(log.ErrorLevel) // Something failed but I'm not quitting.
	} else if *param_log_level == "debug" {
		log.SetLevel(log.DebugLevel) // Useful debugging information
	} else if *param_log_level == "warn" {
		log.SetLevel(log.WarnLevel) // You should probably take a look at this.
	} else if *param_log_level == "trace" {
		log.SetLevel(log.TraceLevel) // Something very low level
	} else {
		log.SetLevel(log.InfoLevel)
	}

	f, errf := os.Create(*param_log_path)
	if errf != nil {
		log.SetOutput(os.Stdout)
		log.Infof("%v starting with %v args ...", os.Args[0], len(os.Args)-1)
		log.Infof("loging in stdout\n")
	} else {
		log.Errorf("error %v", errf)
		multi := io.MultiWriter(f, os.Stdout)
		log.SetOutput(multi)
		log.Infof("%v starting with %v args ...", os.Args[0], len(os.Args)-1)
		log.Infof("loging in file %v", *param_log_path)
	}
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})

	godotenv.Load(".env")
	fmt.Printf("environment = %s \n", os.Getenv("APP_ENV"))

	if os.Getenv("ENVIRONMENT") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	gin.ForceConsoleColor() //gin.DisableConsoleColor()
	// Initialisation du routeur
	r := gin.Default()
	tmpl := template.Must(template.ParseFS(tmplEmbed, "templates/*/*.html"))
	r.SetHTMLTemplate(tmpl)
	r.Static("/static", "./static")
	r.Use(nice.Recovery(router.RecoveryHandler))
	//r.Use(streamlimit.MaxSizeAllowed(100 << 10))
	//r.MaxMultipartMemory = 80 << 20 // 80 MiB
	//r.Use(LoggerMiddleware())
	router.SetupRouterPublic(r)
	router.SetupRouterClient(r)
	router.SetupRouterAdmin(r)

	db.SetupDatabase()
	// Port du serveur
	r.Run(":3000")
	//http.ListenAndServe(":3000",
	//csrf.Protect([]byte("32-byte-long-auth-key"), csrf.Secure(false))(r))
}
