package controllers

import (
	mysqldb "armatel/db"
	"armatel/entity"
	models "armatel/entity"
	"armatel/helpers"
	svc "armatel/services"
	"errors"
	"fmt"
	"log"
	"mime/multipart"
	"net/http"
	"path"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	//"gorm.io/driver/sqlite"

	_ "gorm.io/driver/mysql"
)

func Terrain_ecoutes_view_Get(c *gin.Context) {
	if true {
		id := c.Param("id")
		fmt.Printf("Terrain_ecoutes_view_Get id Param:[%v]\n", id)
		var ecoutes []entity.EcouteEntity
		ecoutes, nb, err := svc.GetEcoutesOfTerrain(id)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		var terr entity.TerrainEntity

		terr, err = svc.GetOneTerrain(id)
		if err == nil {
			fmt.Printf("found terrain: %v\n", terr)
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("Error Terrain_ecoutes_view_Get '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
		}
		c.HTML(http.StatusOK, "ecoutes/index.html", gin.H{
			//"messages":  flashes(c),
			"ecoutes":   ecoutes,
			"terrain":   terr,
			"idTerrain": id,
			"nb":        nb,
			"username":  helpers.Admin_GetUserName(c),
		})
	}
}

const MaxUploadSize = 16 * 1024 * 1024 // 16 MB MEDIUMBLOB: Holds up to 16,777,215 bytes (16 MB)
var allowedExtensions = []string{".wav", ".png", ".gif"}

func validateFile(file *multipart.FileHeader) error {
	// Check file size
	log.Printf("Check file size %v\n", MaxUploadSize)
	if file.Size > MaxUploadSize {
		return errors.New("file size is too large")
	}
	// Check file extension
	ext := strings.ToLower(filepath.Ext(file.Filename))
	for _, allowedExt := range allowedExtensions {
		if ext == allowedExt {
			return nil
		}
	}
	return errors.New("file type not allowed")
}

func Ecoutes_Create_Post(c *gin.Context) {
	fmt.Printf("Ecoutes_Create_Post \n")

	if db, err := mysqldb.GetInstance2(); err == nil {

		if path.Base(c.Request.URL.Path) == "create" {

			// TODO : use shouldBind
			fmt.Printf("Ecoutes_Create_Get CREATE \n")

			/*
				sentiment := c.PostForm("sentiment")
				id_terrain := c.PostForm("id_terrain")
				annotation := c.PostForm("annotation")

				sent, _ := strconv.Atoi(sentiment)
				idt, _ := strconv.Atoi(id_terrain)
				//db.Exec("UPDATE users SET money = ? WHERE name = ?", gorm.Expr("money * ? + ?", 10000, 1), "jinzhu")

				tx := db.Exec("insert into ecoutes set sentiment=?,id_terrain=?,annotation=?", sent, idt, annotation)
				if tx.Error != nil {
					log.Println(err)
					flashMessage(c, fmt.Sprintf("New ecoute '%s' error %v", id_terrain, err))
					c.Redirect(http.StatusFound, "/admin/ecoutes/")
					return*/

			var ecout entity.EcouteEntity
			if errA := c.ShouldBind(&ecout); errA == nil {
				//c.String(http.StatusOK, `the body should be formA`)
				// Always an error is occurred by this because c.Request.Body is EOF now.
				//terr.ID = c.PostForm("id")
				err := svc.SaveOneEcoutes(ecout)
				if err != nil {
					log.Println(err)
					helpers.FlashMessage(c, fmt.Sprintf("Error saving new ecoute  error %v", err))
					c.Redirect(http.StatusFound, "/admin/ecoutes/")
					return
				} else {

					var new_id_ecoutes int64
					sqlStr := "SELECT LAST_INSERT_ID();"
					db.Raw(sqlStr).Scan(&new_id_ecoutes)
					fmt.Printf("new_id_ecoutes: %v\n", new_id_ecoutes)

					file, errfile := c.FormFile("file")
					log.Printf("FormFile successful\n")

					if errfile != nil {

						log.Printf("error c.FormFile ")

						/*ff, fileHeader, err3 := c.Request.FormFile("file")
						dst, err := os.Create(fmt.Sprintf("/tmp/%d%s", time.Now().UnixNano(),
							filepath.Ext(fileHeader.Filename)))
						if err != nil || err3 != nil {
							log.Printf("Create error %v\n", err3, err)
							return
						}

						defer dst.Close()

						_, err = io.Copy(dst, ff)
						if err != nil {
							log.Printf("Copy error %v\n", err3, err)
							return
						}

						log.Printf("Upload successful\n")*/

					} else {
						log.Printf("file.Size %v\n", file.Size)

						fmt.Printf("file.Filename %v\n", file.Filename)
						filename := filepath.Base(file.Filename)
						fmt.Printf("filepath.Base %v\n", filename)
						extension := filepath.Ext(file.Filename)
						fmt.Printf("extension %v\n", extension)
						// Generate random file name for the new uploaded file so it doesn't override the old file with same name
						//newFileName := "/tmp/upload_logo_" + id_terrain + "_" + uuid.New().String() + extension
						newFileName := "/tmp/audio_file_" + uuid.New().String() + extension
						log.Printf("newFileName %v\n", newFileName)

						err := validateFile(file)
						if err != nil { // 400 BAD REQUEST
							c.String(http.StatusBadRequest, "File upload failed: %s", err.Error())
							log.Printf("File upload failed: %s", err.Error())
							return
						}

						err = c.SaveUploadedFile(file, newFileName)
						if err != nil {
							fmt.Println("err SaveUploadedFile:", err)
							return
						} else {
							fmt.Println("OK SaveUploadedFile:", newFileName)
						}

						/*
							data, err := os.ReadFile(newFileName)
							if err != nil {
								fmt.Println("err ReadFile:", err)
								return
							}

							contentType := http.DetectContentType(data)
							fmt.Printf("contentType: %v\n", contentType)

							if strings.Contains(strings.ToLower(contentType), "audio") */
						if true {
							//imgBase64Str := base64.StdEncoding.EncodeToString(data)
							//fmt.Printf("imgBase64Str: %v\n", imgBase64Str)

							sql := ""
							sql = fmt.Sprintf("insert into audios set mediadata=LOAD_FILE('%s');", newFileName)
							fmt.Printf("sql: %v\n", sql)
							tx := db.Exec(sql)
							if tx.Error != nil {
								log.Println(err)
								helpers.FlashMessage(c, fmt.Sprintf("Error updating mediadata audios %s error %v", newFileName, err))
								c.Redirect(http.StatusFound, "/admin/terrains/")
								return
							} else {
								helpers.FlashMessage(c, fmt.Sprintf("OK %v", sql))
								var new_id_audio int64
								sqlStr := "SELECT LAST_INSERT_ID();"
								db.Raw(sqlStr).Scan(&new_id_audio)
								fmt.Printf("new_id_audio: %v\n", new_id_audio)

								tx = db.Exec("update ecoutes set id_audio=? where id=?", new_id_audio, new_id_ecoutes)
								if tx.Error != nil {
									helpers.FlashMessage(c, fmt.Sprintf("NOK %v", sql))
								} else {
									helpers.FlashMessage(c, fmt.Sprintf("importing mediadata audios %v ecoute '%v' successfully.", new_id_audio, new_id_ecoutes))
								}
							}

						} else {
							helpers.FlashMessage(c, fmt.Sprintf("bad contentType %v", file.Header))
						}

					}
				}
			}
			//sqlDB, _ := db.DB()		sqlDB.Close()
			//helpers.FlashMessage(c, fmt.Sprintf("New ecoute '%s' created successfully.", sentiment))
			c.Redirect(http.StatusFound, "/admin/ecoutes/")
		} else {
			id := c.Param("id")
			fmt.Printf("Ecoutes_Create_Get UPDATE:[%v]\n", id)
			ecout, err := svc.GetOneEcoute(id)
			if err != nil {
				helpers.FlashMessage(c, fmt.Sprintf("Error GetOneEcoute '%v' error %v", id, err))
				c.Redirect(http.StatusFound, "/admin/ecoutes/")
				return

			}
			if errA := c.ShouldBind(&ecout); errA == nil {
				//c.String(http.StatusOK, `the body should be formA`)
				// Always an error is occurred by this because c.Request.Body is EOF now.
				//terr.ID = c.PostForm("id")
				err := svc.SaveOneEcoutes(ecout)
				if err != nil {
					log.Println(err)
					helpers.FlashMessage(c, fmt.Sprintf("Error saving ecoute '%v' error %v", id, err))
					c.Redirect(http.StatusFound, "/admin/ecoutes/")
					return
				}
				helpers.FlashMessage(c, fmt.Sprintf("OK saving ecoutes '%v' %v", id, ecout))
				c.Redirect(http.StatusFound, "/admin/ecoutes/")
				//c.Redirect(http.StatusFound, fmt.Sprintf("/admin/terrains/view/%v", id))
			} else {
				helpers.FlashMessage(c, fmt.Sprintf("NOK saving ecoutes '%v' %v", id, errA))
				c.Redirect(http.StatusFound, "/admin/ecoutes/")
			}
		}

	}
}

/*
[root@control-master tmp]# docker run -ti --rm --memory 1G --tmpfs /tmp:rw,size=1G,mode=1777 -v $(pwd)/go:/go golang:1.16.5 /bin/bash

root@b672a98e5314:/go/testmodules# cat main.go
package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"os"
	"path/filepath"
)

func main() {
	engine := gin.Default()
	engine.POST("/streamupload", func(c *gin.Context) {
		if c.GetHeader("Content-Type") != "application/octet-stream" {
			err := fmt.Errorf("required octet-stream")
			c.AbortWithStatusJSON(400, map[string]string{"message": err.Error()})
			return
		}
		info, err := os.Create(filepath.Join("/home", "foo"))
		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"message": "Create: "+err.Error()})
			return
		}
		defer info.Close()
		_, err = io.Copy(info, c.Request.Body)
		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"message": "Copy: "+err.Error()})
			return
		}
		c.JSON(200, map[string]string{"message": "ok stream"})
	})

	if err := engine.Run("0.0.0.0:19090"); err != nil {
		panic(err)
	}
}

root@b53810b3e294:/go/testmodules# GOTMPDIR=/opt/ go run main.go

[root@control-master ~]# dd if=/dev/zero of=5G bs=1M count=5170 status=progress
[root@control-master ~]# curl -vvv -H "Content-Type:application/octet-stream" -T 5G -X POST 172.17.0.2:19090/streamupload

*/

func Ecoutes_Create_Update_Delete_Get(c *gin.Context) { // CREATE or UPDATE depending on url

	var ecout entity.EcouteEntity
	var err error
	var title, isCreation string

	urlpath := c.Request.URL.Path
	fmt.Println("Ecoutes_Create_Update_Delete_Get: ", c.Request.Host)
	fmt.Println("The URL.Path: ", c.Request.URL.Path)
	fmt.Println("The URL.path.Base: ", path.Base(c.Request.URL.Path))

	if strings.Contains(urlpath, "create") {
		fmt.Printf("Ecoutes_Create_Update_Delete_Get CREATE \n")
		title = "Formulaire de création d'une écoute"
		isCreation = "yes"
	} else if strings.Contains(urlpath, "delete") {
		id := c.Param("id")
		fmt.Printf("Ecoutes_Create_Update_Delete_Get DELETE:[%v]\n", id)
		ecout, err = svc.GetOneEcoute(id)
		if err == nil {
			err = svc.DeleteOneEcoute(ecout)
			if err == nil {
				helpers.FlashMessage(c, fmt.Sprintf("Suppression de l 'écoute %v", id))
				c.Redirect(http.StatusFound, "/admin/ecoutes/")
			} else {
				helpers.FlashMessage(c, fmt.Sprintf("GetOneEcoute error %v", err))
			}
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("DeleteOneEcoute error %v", err))
		}
	} else if strings.Contains(urlpath, "update") {
		id := c.Param("id")
		fmt.Printf("Ecoutes_Create_Update_Delete_Get UPDATE:[%v]\n", id)
		ecout, err = svc.GetOneEcoute(id)
		title = "Formulaire de modification d'une écoute"
		isCreation = "no"
	} else {
		helpers.FlashMessage(c, fmt.Sprintf("Error urlpath %v", urlpath))
	}

	var terrLists []models.TerrainEntity
	terrLists, terrCount, err := svc.GetAllTerrains()
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error GetAllTerrains %v", err))
	}
	c.HTML(http.StatusOK, "ecoutes/formulaire.html", gin.H{
		//"messages": flashes(c),
		"terrains": terrLists, //  passer  tous les id terrain + nom
		"ecoute":   ecout,
		"nb":       terrCount,
		"username": helpers.Admin_GetUserName(c),
		"title":    title,
		"creation": isCreation,
	})
}

func Ecoutes_Get(c *gin.Context) {
	var ecout_arr []entity.EcouteEntity
	var nb int64
	var err error
	ecout_arr, nb, err = svc.GetAllEcoutes()

	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error GetAllEcoutes %v", err))
	}

	c.HTML(http.StatusOK, "ecoutes/all.html", gin.H{
		//"messages": flashes(c),
		"ecoutes":  ecout_arr,
		"nb":       nb,
		"username": helpers.Admin_GetUserName(c),
	})
}
