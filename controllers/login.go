package controllers

import (
	models "armatel/entity"
	"armatel/helpers"
	svc "armatel/services"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/csrf"
)

func Admin_LoginForm_GET(c *gin.Context) {

	helpers.Client_ClearSession(c) // fin session client si en cours

	// Récupération du nom d'utilisateur pour le templating
	username := helpers.Admin_GetUserName(c)

	c.HTML(200, "login/formadmin.html", gin.H{
		"username":       username,
		"currentPage":    "formadmin",
		"warning":        helpers.GetFlashCookie(c, "warning"),
		csrf.TemplateTag: csrf.TemplateField(c.Request),
	})
}

func Client_LoginForm_GET(c *gin.Context) {

	helpers.Admin_ClearSession(c) // fin session admin si en cours

	// Récupération du nom d'utilisateur pour le templating
	username := helpers.Client_GetUserName(c)

	c.HTML(200, "login/formclient.html", gin.H{
		"username":       username,
		"currentPage":    "formclient",
		"warning":        helpers.GetFlashCookie(c, "warning"),
		csrf.TemplateTag: csrf.TemplateField(c.Request),
	})
}

func Admin_Login_POST(c *gin.Context) {
	// Récupération des champs
	username := c.PostForm("username")
	clearPassword := c.PostForm("password")

	user, err := svc.GetOneUserByName(username)
	if err != nil {
		//helpers.FlashMessage(c, fmt.Sprintf("Error Admin_Login_POST %v %v", username, err))
		helpers.SetFlashCookie(c, "warning", fmt.Sprintf("Error Admin_Login_POST %v %v", username, err))
		//c.Redirect(302, "/admin/login")
		c.Redirect(302, "/")
		return
	}

	h := sha1.New()
	h.Write([]byte(clearPassword))
	sha1_hash := hex.EncodeToString(h.Sum(nil))

	log.Printf("sha1_hash=%v\n", sha1_hash)
	log.Printf("user.Cryptpwd=%v\n", user.CryptPwd)

	if sha1_hash == user.CryptPwd {
		// Vérification du mdp
		//if clearPassword == "secret" && username == "admin"
		// Création du cookie de session
		helpers.Admin_SetSession(username, c)
		helpers.SetFlashCookie(c, "success", "Bienvenue "+username)
		user, err := svc.GetOneUserByName(username)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error GetOneUserByName %v %v", username, err))
		} else {
			user.LastLoggedAt = time.Now()
			err2 := svc.SaveOneUser(user)
			if err2 != nil {
				helpers.FlashMessage(c, fmt.Sprintf("Error SaveOneUser %v", err2))
			} else {
				c.HTML(200, "login/homeadmin.html", gin.H{
					"username":    username,
					"currentPage": "homeadmin",
					"user":        user,
					"success":     helpers.GetFlashCookie(c, "success"),
				})
			}
		}
		//c.Redirect(302, "/admin/home")
	} else {
		// MDP incorrect
		helpers.SetFlashCookie(c, "warning", "Mot de passe incorrect")
		c.Redirect(302, "/admin/login")
	}
}

func Client_Login_POST(c *gin.Context) {
	// Récupération des champs : username = id terrain
	id_terrain := c.PostForm("idterrain")
	clearPassword := c.PostForm("password")

	var terr models.TerrainEntity
	terr, err := svc.GetOneTerrain(id_terrain)
	if err != nil {
		//helpers.FlashMessage(c, fmt.Sprintf("Error Client_Login_POST '%d' error %v", id_terrain, err))
		helpers.SetFlashCookie(c, "warning", "Error Client_Login_POST "+id_terrain)
		c.Redirect(http.StatusFound, "/")
		return
	}

	// Vérification du mdp

	h := sha1.New()
	h.Write([]byte(clearPassword))
	sha1_hash := hex.EncodeToString(h.Sum(nil))

	log.Printf("id_terrain=%v\n", id_terrain)
	log.Printf("sha1_hash=%v\n", sha1_hash)
	log.Printf("terr.Cryptpwd=%v\n", terr.Cryptpwd)

	if sha1_hash == terr.Cryptpwd {
		// Création du cookie de session
		helpers.Client_SetSession(id_terrain, c)
		helpers.SetFlashCookie(c, "success", "Bienvenue "+id_terrain)
		c.Redirect(302, "/client/home")
	} else {
		// MDP incorrect
		helpers.SetFlashCookie(c, "warning", "Mot de passe incorrect")
		c.Redirect(302, "/client/home")
	}
}

func Admin_Logout_GET(c *gin.Context) {
	helpers.Admin_ClearSession(c)
	helpers.SetFlashCookie(c, "success", "Vous êtes désormais déconnecté(e)")
	c.Redirect(302, "/")
}

func Client_Logout_GET(c *gin.Context) {
	helpers.Client_ClearSession(c)
	helpers.SetFlashCookie(c, "success", "Vous êtes désormais déconnecté(e)")
	c.Redirect(302, "/")
}

func Admin_Client_Logout_GET(c *gin.Context) {
	helpers.Client_ClearSession(c)
	helpers.Admin_ClearSession(c)
	helpers.SetFlashCookie(c, "success", "Vous êtes désormais déconnecté(e)")
	c.Redirect(302, "/")
}

func Admin_test_Secure_Admin_GET(c *gin.Context) {
	// Récupération du nom d'utilisateur pour le templating
	userName := helpers.Admin_GetUserName(c)
	c.HTML(200, "login/privateadmin.html", gin.H{
		"username":    userName,
		"currentPage": "privateadmin",
		//"users":       users,
		"success": helpers.GetFlashCookie(c, "success"),
	})
}

func Admin_Secure_Admin_GET(c *gin.Context) {
	// Récupération du nom d'utilisateur pour le templating
	userName := helpers.Admin_GetUserName(c)

	c.HTML(200, "login/homeadmin.html", gin.H{
		"username":    userName,
		"currentPage": "homeadmin",
		//"users":       users,
		"success": helpers.GetFlashCookie(c, "success"),
	})
}
func Client_Secure_Admin_GET(c *gin.Context) {
	// Récupération du nom d'utilisateur pour le templating
	userName := helpers.Client_GetUserName(c)
	c.HTML(200, "login/homeclient.html", gin.H{
		"username":    userName,
		"currentPage": "homeclient",
		//"users":       users,
		"success": helpers.GetFlashCookie(c, "success"),
	})
}

func Client_test_Secure_Admin_GET(c *gin.Context) {
	// Récupération du nom d'utilisateur pour le templating
	userName := helpers.Client_GetUserName(c)
	c.HTML(200, "login/privateclient.html", gin.H{
		"username":    userName,
		"currentPage": "privateclient",
		//"users":       users,
		"success": helpers.GetFlashCookie(c, "success"),
	})
}

func PublicRootPage_GET(c *gin.Context) {
	userName := helpers.Client_GetUserName(c)
	c.HTML(200, "login/publicpage.html", gin.H{
		"username":    userName,
		"currentPage": "publicpage",
		"success":     helpers.GetFlashCookie(c, "success"),
		"warning":     helpers.GetFlashCookie(c, "warning"),
	})
}
