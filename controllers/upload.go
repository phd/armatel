package controllers

import (
	mysqldb "armatel/db"
	"armatel/helpers"
	"bytes"
	"encoding/csv"
	"fmt"
	"image/jpeg"
	"image/png"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"

	//"gorm.io/driver/sqlite"
	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Terrain_uploadlogo_Get(c *gin.Context) {
	fmt.Printf("terrain_uploadlogo_Get\n")
	id := c.Param("id")
	fmt.Printf("terrain_uploadlogo_Get id:[%v]\n", id)
	c.HTML(http.StatusOK, "terrains/logo.html", gin.H{
		//helpers.flashmessage,
		"id":       id,
		"username": helpers.Admin_GetUserName(c),
	})
}

func Terrain_uploadlogo2_Get(c *gin.Context) {
	fmt.Printf("Terrain_uploadlogo2_Get\n")
	id := c.Param("id")
	fmt.Printf("Terrain_uploadlogo2_Get id:[%v]\n", id)
	c.HTML(http.StatusOK, "terrains/uploadlogo2.html", gin.H{
		//helpers.flashmessage,
		"id":       id,
		"username": helpers.Admin_GetUserName(c),
	})
}

func Terrain_uploadlogo1_Get(c *gin.Context) {
	fmt.Printf("Terrain_uploadlogo1_Get\n")
	id := c.Param("id")
	fmt.Printf("Terrain_uploadlogo1_Get id:[%v]\n", id)
	c.HTML(http.StatusOK, "terrains/uploadlogo1.html", gin.H{
		//helpers.flashmessage,
		"id":       id,
		"username": helpers.Admin_GetUserName(c),
	})
}

func Terrain_uploadcsv_Post(c *gin.Context) {
	fmt.Printf("\n")
	fmt.Printf("\n")
	fmt.Printf("terrain_uploadcsv_Post\n")

	file, _ := c.FormFile("file")
	id := c.PostForm("id")
	fmt.Printf("id %v\n", id)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("file.Size %v\n", file.Size)

	// Retrieve file information
	extension := filepath.Ext(file.Filename)
	fmt.Printf("extension %v\n", extension)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := "/tmp/csv_" + uuid.New().String() + extension
	fmt.Printf("newFileName %v\n", newFileName)

	var contentType string
	err := c.SaveUploadedFile(file, newFileName)
	if err != nil {
		fmt.Printf("err SaveUploadedFile:", err)
		return
	}

	fmt.Println("OK SaveUploadedFile:", newFileName)
	data, err := os.ReadFile(newFileName)
	if err != nil {
		fmt.Println("err ReadFile:", err)
		helpers.FlashMessage(c, fmt.Sprintf("Error file.Open '%v' error %v", file.Filename, err))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}

	contentType = http.DetectContentType(data)
	fmt.Printf("contentType %v\n", contentType) // contentType text/plain; charset=utf-8

	fmt.Printf("\n")
	fmt.Printf("\n")
	c.Redirect(http.StatusFound, "/admin/terrains/")

	if strings.Contains(contentType, "text/plain") {

		file, err := os.Open(newFileName) //"Sample.csv")

		// Checks for the error
		if err != nil {
			fmt.Printf("Error while reading the file %v\n", err)
		}

		// Closes the file
		defer file.Close()

		// The csv.NewReader() function is called in
		// which the object os.File passed as its parameter
		// and this creates a new csv.Reader that reads
		// from the file
		reader := csv.NewReader(file)

		// To specify the custom separator use the
		// following syntax
		// Comma is the field delimiter. By default it is
		// set to comma (',') by NewReader.
		// Comma must be a valid rune (int32) and must not be
		// \r, \n, or the Unicode replacement character (0xFFFD).
		reader.Comma = '-'

		// ReadAll reads all the records from the CSV file and
		// Returns them as slice of slices of string and an
		// error if any
		records, err := reader.ReadAll()

		// Checks for the error
		if err != nil {
			fmt.Println("Error reading records")
		}

		// Loop to iterate through
		// and print each of the string slice
		for _, eachrecord := range records {
			fmt.Println(eachrecord)
		}
	} else if strings.Contains(contentType, "text/csv") {
		fileContent, err := file.Open()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		xlsx, err := excelize.OpenReader(fileContent)
		if err != nil {
			//c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			helpers.FlashMessage(c, fmt.Sprintf("Error excelize.OpenReader '%v' error %v", file.Filename, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		} // Extract data from an Excel file and insert it into a database
		rows := xlsx.GetRows("Sheet1")
		for i, row := range rows {
			if i == 0 {
				// Skip header row ?
				//continue
			}
			code := row[0]
			nom := row[1]
			fmt.Printf("read excel line: %v %v", code, nom)
			// Save data to the database
			/*	_, err := db.Exec("INSERT INTO departements (code, nom) VALUES (?, ?)", code, nom)
				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
					return
				}
			*/
		}
	}
	helpers.FlashMessage(c, fmt.Sprintf("OK file.Open '%v'", file.Filename))
	c.Redirect(http.StatusFound, "/admin/terrains/")

}

func Terrain_importcsv_Get(c *gin.Context) {
	fmt.Printf("terrain_importcsv_Get\n")
	//id := c.Param("id")
	//fmt.Printf("terrain_importcsv_Get id:[%v]\n", id)
	c.HTML(http.StatusOK, "terrains/csv.html", gin.H{
		//helpers.flashmessage,
		//"id":       id,
		"username": helpers.Admin_GetUserName(c),
	})
}

func Terrain_uploadlogo_Post2(c *gin.Context) {
	/* curl -X POST http://localhost:8080/uploadlogo -F "file=../path-to-file/file.jpg" -H "Content-Type: multipart/form-data"	 */
	fmt.Printf("terrain_uploadlogo_Post\n")

	file, _ := c.FormFile("file")
	id_terrain := c.PostForm("id")
	fmt.Printf("id_terrain %v\n", id_terrain)
	fmt.Printf("file.Size %v\n", file.Size)
	fmt.Printf("file.Filename %v\n", file.Filename)

	filename := filepath.Base(file.Filename)

	fmt.Printf("filepath.Base %v\n", filename)

	// Retrieve file information
	extension := filepath.Ext(file.Filename)
	fmt.Printf("extension %v\n", extension)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	//newFileName := "/tmp/upload_logo_" + id_terrain + "_" + uuid.New().String() + extension
	newFileName := "/tmp/" + uuid.New().String() + extension
	fmt.Printf("newFileName %v\n", newFileName)

	err := c.SaveUploadedFile(file, newFileName)
	if err != nil {
		fmt.Println("err SaveUploadedFile:", err)
		return
	} else {
		fmt.Println("OK SaveUploadedFile:", newFileName)
	}

	//err = os.WriteFile(fname, []byte{1, 2}, 0666)
	data, err := os.ReadFile(newFileName)
	if err != nil {
		fmt.Println("err ReadFile:", err)
		return
	}

	contentType := http.DetectContentType(data)

	switch contentType {
	case "image/png":
		fmt.Println("Image type is already PNG.")
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(data))
		if err != nil {
			fmt.Printf("unable to decode jpeg: %w", err)
		}

		var buf bytes.Buffer
		if err := png.Encode(&buf, img); err != nil {
			fmt.Printf("unable to encode png: %w", err)
		}
		data = buf.Bytes()
	default:
		fmt.Printf("unsupported content typo: %s", contentType)
	}
	if true {
		//imgBase64Str := base64.StdEncoding.EncodeToString(data)
		//fmt.Printf("imgBase64Str: %v\n", imgBase64Str)

		if db, err := mysqldb.GetInstance(); err == nil {
			sql := ""
			if false {
				//update employees set file=LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/fileName.jpg') where id = 1;
				sql = fmt.Sprintf("update terrains set picture=LOAD_FILE('%s') where id=%s;", newFileName, id_terrain)
				fmt.Printf("sql: %v\n", sql)

			} else {
				sql = fmt.Sprintf("insert into logos set logo=LOAD_FILE('%s');", newFileName)
				fmt.Printf("sql: %v\n", sql)
			}
			_, err = db.Exec(sql)
			if err != nil {
				log.Println(err)
				helpers.FlashMessage(c, fmt.Sprintf("Error updating logo terrain '%d' error %v", id_terrain, err))
				c.Redirect(http.StatusFound, "/admin/terrains/")
				return
			} else {
				helpers.FlashMessage(c, fmt.Sprintf("OK %v", sql))
			}
		}
	}
	helpers.FlashMessage(c, fmt.Sprintf("importing logo terrain %s from '%v' successfully.", id_terrain, newFileName))
	c.Redirect(http.StatusFound, "/about")
}

func Gorm_terrain_uploadlogo_Post(c *gin.Context) {
	/* curl -X POST http://localhost:8080/uploadlogo -F "file=../path-to-file/file.jpg" -H "Content-Type: multipart/form-data"	 */
	fmt.Printf("terrain_uploadlogo_Post\n")

	file, _ := c.FormFile("file")
	id := c.PostForm("id")
	fmt.Printf("id %v\n", id)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("file.Size %v\n", file.Size)

	filename := filepath.Base(file.Filename)
	fmt.Println("file.Filename %v\n", file.Filename)
	fmt.Println("filepath.Base %v\n", filename)

	// Retrieve file information
	extension := filepath.Ext(file.Filename)
	fmt.Println("extension %v\n", extension)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := "/tmp/" + uuid.New().String() + extension
	fmt.Println("newFileName %v\n", newFileName)

	err := c.SaveUploadedFile(file, newFileName)
	if err != nil {
		fmt.Println("err SaveUploadedFile:", err)
		return
	} else {
		fmt.Println("OK SaveUploadedFile:", newFileName)
	}

	//err = os.WriteFile(fname, []byte{1, 2}, 0666)
	data, err := os.ReadFile(newFileName)
	if err != nil {
		fmt.Println("err ReadFile:", err)
		return
	}

	contentType := http.DetectContentType(data)

	switch contentType {
	case "image/png":
		fmt.Println("Image type is already PNG.")
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(data))
		if err != nil {
			fmt.Printf("unable to decode jpeg: %w", err)
		}

		var buf bytes.Buffer
		if err := png.Encode(&buf, img); err != nil {
			fmt.Printf("unable to encode png: %w", err)
		}
		data = buf.Bytes()
	default:
		fmt.Printf("unsupported content typo: %s", contentType)
	}
	if true {
		//imgBase64Str := base64.StdEncoding.EncodeToString(data)
		//fmt.Printf("imgBase64Str: %v\n", imgBase64Str)

		db, err := gorm.Open(mysql.Open(os.Getenv("DB_URL")), &gorm.Config{})
		if err != nil {
			fmt.Printf("error open db: %v\n", err)
			return
		}

		//update employees set file=LOAD_FILE('C:/ProgramData/MySQL/MySQL Server 5.7/Uploads/fileName.jpg') where id = 1;
		sql := fmt.Sprintf("update terrains set picture=LOAD_FILE('%s') where id=%s", newFileName, id)
		fmt.Printf("sql: %v\n", sql)
		//_, err = db.Exec(sql)
		//db.Raw("UPDATE users SET terrains set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		//db.Exec("UPDATE terrains SET set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		db.Exec(sql)
		if err != nil {
			log.Println(err)
			helpers.FlashMessage(c, fmt.Sprintf("Error updating logo terrain '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")

			return
		}
		sqlDB, err := db.DB()
		sqlDB.Close()

	}
	helpers.FlashMessage(c, fmt.Sprintf("importing logo terrain from '%v' successfully.", newFileName))

	c.HTML(http.StatusOK, "terrains/autoclose.html", gin.H{})
	//c.Redirect(http.StatusFound, "/admin/terrains//")
}

func Gorm_terrain_uploadlogo2_Post(c *gin.Context) {

	file, _ := c.FormFile("file")
	id_terrain := c.PostForm("id")
	fmt.Printf("Gorm_terrain_uploadlogo2_Post id_terrain %v\n", id_terrain)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("file.Size %v\n", file.Size)

	filename := filepath.Base(file.Filename)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("filepath.Base %v\n", filename)

	// Retrieve file information
	extension := filepath.Ext(file.Filename)
	fmt.Printf("extension %v\n", extension)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := "/tmp/" + uuid.New().String() + extension
	fmt.Printf("newFileName %v\n", newFileName)

	err := c.SaveUploadedFile(file, newFileName)
	if err != nil {
		fmt.Println("err SaveUploadedFile:", err)
		return
	} else {
		fmt.Println("OK SaveUploadedFile:", newFileName)
	}

	//err = os.WriteFile(fname, []byte{1, 2}, 0666)
	data, err := os.ReadFile(newFileName)
	if err != nil {
		fmt.Println("err ReadFile:", err)
		return
	}

	contentType := http.DetectContentType(data)

	switch contentType {
	case "image/png":
		fmt.Println("Image type is already PNG.")
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(data))
		if err != nil {
			fmt.Printf("unable to decode jpeg: %w", err)
		}

		var buf bytes.Buffer
		if err := png.Encode(&buf, img); err != nil {
			fmt.Printf("unable to encode png: %w", err)
		}
		data = buf.Bytes()
	default:
		fmt.Printf("unsupported content typo: %s", contentType)
	}
	if true {
		//imgBase64Str := base64.StdEncoding.EncodeToString(data)
		//fmt.Printf("imgBase64Str: %v\n", imgBase64Str)

		db, err := gorm.Open(mysql.Open(os.Getenv("DB_URL")), &gorm.Config{})
		if err != nil {
			fmt.Printf("error open db: %v\n", err)
			return
		}

		/*
			 var ID int64
			tx.Raw("INSERT INTO yourTable (yourColumn) VALUES ('testInsertValue') RETURNING id").Scan(&ID)
		*/
		sql := fmt.Sprintf("insert into logos set logo=LOAD_FILE('%s') ", newFileName)
		//db.Raw("UPDATE users SET terrains set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		//db.Exec("UPDATE terrains SET set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		tx := db.Exec(sql)
		if tx.Error != nil {
			fmt.Printf("NOK sql: %v\n", sql)
			log.Println(tx.Error)
			return
		}
		fmt.Printf("OK sql: %v\n", sql)

		var new_id_logo int64
		sqlStr := "SELECT LAST_INSERT_ID();"
		tx = db.Raw(sqlStr).Scan(&new_id_logo)
		//tx.Raw("INSERT INTO t2 (id) VALUES (2),(3) RETURNING id").Scan(&ID)
		//tx := db.Begin()
		//tx.Raw(sql).Scan(&new_id_logo)

		if tx.Error != nil {
			log.Println(err)
			fmt.Printf("NOK sql: %v\n", sql)
			helpers.FlashMessage(c, fmt.Sprintf("Error updating logo terrain '%d' error %v", id_terrain, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		} else {
			sql := fmt.Sprintf("update terrains set id_logo2=%v where id=%s", new_id_logo, id_terrain)
			fmt.Printf("OK sql: %v\n", sql)
			tx = db.Exec(sql)
		}
		tx.Commit()
		sqlDB, err := db.DB()
		sqlDB.Close()

	}
	helpers.FlashMessage(c, fmt.Sprintf("importing logo terrain from '%v' successfully.", newFileName))

	c.HTML(http.StatusOK, "terrains/autoclose.html", gin.H{})
	//c.Redirect(http.StatusFound, "/admin/terrains//")
}

func Gorm_terrain_uploadlogo1_Post(c *gin.Context) {

	file, _ := c.FormFile("file")
	id_terrain := c.PostForm("id")
	fmt.Printf("id_terrain %v\n", id_terrain)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("file.Size %v\n", file.Size)

	filename := filepath.Base(file.Filename)
	fmt.Printf("file.Filename %v\n", file.Filename)
	fmt.Printf("filepath.Base %v\n", filename)

	// Retrieve file information
	extension := filepath.Ext(file.Filename)
	fmt.Printf("extension %v\n", extension)
	// Generate random file name for the new uploaded file so it doesn't override the old file with same name
	newFileName := "/tmp/" + uuid.New().String() + extension
	fmt.Printf("newFileName %v\n", newFileName)

	err := c.SaveUploadedFile(file, newFileName)
	if err != nil {
		fmt.Printf("err SaveUploadedFile:", err)
		return
	} else {
		fmt.Printf("OK SaveUploadedFile:", newFileName)
	}

	//err = os.WriteFile(fname, []byte{1, 2}, 0666)
	data, err := os.ReadFile(newFileName)
	if err != nil {
		fmt.Printf("err ReadFile:", err)
		return
	}

	contentType := http.DetectContentType(data)

	switch contentType {
	case "image/png":
		fmt.Printf("Image type is already PNG.")
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(data))
		if err != nil {
			fmt.Printf("unable to decode jpeg: %w", err)
		}

		var buf bytes.Buffer
		if err := png.Encode(&buf, img); err != nil {
			fmt.Printf("unable to encode png: %w", err)
		}
		data = buf.Bytes()
	default:
		fmt.Printf("unsupported content typo: %s", contentType)
	}
	if true {
		//imgBase64Str := base64.StdEncoding.EncodeToString(data)
		//fmt.Printf("imgBase64Str: %v\n", imgBase64Str)

		db, err := gorm.Open(mysql.Open(os.Getenv("DB_URL")), &gorm.Config{})
		if err != nil {
			fmt.Printf("error open db: %v\n", err)
			return
		}
		/*var ID int64
		tx.Raw("INSERT INTO yourTable (yourColumn) VALUES ('testInsertValue') RETURNING id").Scan(&ID)
		*/
		sql := fmt.Sprintf("insert into logos set logo=LOAD_FILE('%s') ", newFileName)
		//db.Raw("UPDATE users SET terrains set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		//db.Exec("UPDATE terrains SET set picture=LOAD_FILE('?') WHERE age = ?", newFileName, id)
		tx := db.Exec(sql)
		if tx.Error != nil {
			fmt.Printf("NOK sql: %v err %v\n", sql, tx.Error)
			return
		}
		fmt.Printf("OK sql: %v\n", sql)

		var new_id_logo int64
		sqlStr := "SELECT LAST_INSERT_ID();"
		tx = db.Raw(sqlStr).Scan(&new_id_logo)
		//tx.Raw("INSERT INTO t2 (id) VALUES (2),(3) RETURNING id").Scan(&ID)
		//tx := db.Begin()
		//tx.Raw(sql).Scan(&new_id_logo)

		if tx.Error != nil {
			fmt.Printf("NOK sql: %v err %v\n", sql, err)
			helpers.FlashMessage(c, fmt.Sprintf("Error updating logo terrain '%d' error %v", id_terrain, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		} else {
			sql := fmt.Sprintf("update terrains set id_logo1=%v where id=%s", new_id_logo, id_terrain)
			fmt.Printf("OK sql: %v\n", sql)
			tx = db.Exec(sql)
		}
		tx.Commit()
		sqlDB, err := db.DB()
		sqlDB.Close()

	}
	helpers.FlashMessage(c, fmt.Sprintf("OK import logo1 terrain from '%v' successfully.", newFileName))

	c.HTML(http.StatusOK, "terrains/autoclose.html", gin.H{})
	//c.Redirect(http.StatusFound, "/admin/terrains//")
}
