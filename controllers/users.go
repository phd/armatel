package controllers

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"

	models "armatel/entity"
	"armatel/helpers"
	svc "armatel/services"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func Create_User_Get(c *gin.Context) {
	fmt.Printf("Create_User_Get \n")
	c.HTML(http.StatusOK, "users/create.html", gin.H{
		//"messages": flashes(c),
		"username": helpers.Admin_GetUserName(c),
	})
}

func Create_User_Post(c *gin.Context) {
	fmt.Printf("Create_User_Post \n")
	var user models.UserEntity
	if errA := c.ShouldBind(&user); errA == nil {
		//c.String(http.StatusOK, `the body should be formA`)
		// Always an error is occurred by this because c.Request.Body is EOF now.
		//terr.ID = c.PostForm("id")
		newpwd := c.PostForm("newpwd")
		h := sha1.New()
		h.Write([]byte(newpwd))
		sha1_hash := hex.EncodeToString(h.Sum(nil))
		user.CryptPwd = sha1_hash
		err := svc.CreateOneUser(user)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error CreateOneUser %v", errA))
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("OK CreateOneUser %v", user.Username))
		}
	} else {
		helpers.FlashMessage(c, fmt.Sprintf("Error ShouldBind %v", errA))
	}
	c.Redirect(302, "/admin/users")
}

func Update_User_Get(c *gin.Context) {
	id := c.Param("id")
	fmt.Printf("Update_User_Get id Param:[%v]\n", id)
	user, err := svc.GetOneUser(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error GetOneUser %v %v", id, err))
		c.Redirect(302, "/admin/users")
	} else {
		var terrLists []models.TerrainEntity
		terrLists, terrCount, err := svc.GetAllTerrains()
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("GetAllTerrains error %v", err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		c.HTML(http.StatusOK, "users/update.html", gin.H{
			"user":      user,
			"terrains":  terrLists,
			"terrCount": terrCount,
			"username":  helpers.Admin_GetUserName(c),
		})
	}
}

func Update_User_Post(c *gin.Context) {
	id_user := c.Param("id")
	log.Printf("Update_User_Post id_user Param:[%v]\n", c.PostForm("id"))
	log.Printf("Update_User_Post id_terrain Param:[%v]\n", c.PostForm("id_terrain"))
	user, err := svc.GetOneUser(id_user)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error GetOneUser %v %v", id_user, err))
	} else {
		if errA := c.ShouldBind(&user); errA == nil {
			//c.String(http.StatusOK, `the body should be formA`)
			// Always an error is occurred by this because c.Request.Body is EOF now.
			//terr.ID = c.PostForm("id")
			newpwd := c.PostForm("newpwd")
			if len(newpwd) > 3 {
				h := sha1.New()
				h.Write([]byte(newpwd))
				sha1_hash := hex.EncodeToString(h.Sum(nil))
				user.CryptPwd = sha1_hash
				fmt.Printf("Update_User_Post newpwd:[%v]\n", newpwd)
			} else {
				fmt.Printf("Update_User_Post unchanged PWD sha1_hash:[%v]\n", user.CryptPwd)
			}
			err := svc.SaveOneUser(user)
			if err != nil {
				helpers.FlashMessage(c, fmt.Sprintf("Error SaveOneUser %v", err))
				c.Redirect(302, "/admin/users")
				return
			} else {

				id_terrain, err1 := strconv.Atoi(c.PostForm("id_terrain"))
				id_user_no, err2 := strconv.Atoi(c.PostForm("id"))
				if err1 == nil && err2 == nil {

					fmt.Printf("Update_User_Post id_terrain Param:[%v]\n", id_terrain)
					var map_user_terr models.UserTerrainEntity
					map_user_terr.IdTerrain = id_terrain
					map_user_terr.IdUser = id_user_no
					svc.CreateOneUserTerrainMap(map_user_terr)
				} else {
					log.Printf("Update_User_Post err:[%v %v]\n", err1, err2)
				}

				helpers.FlashMessage(c, fmt.Sprintf("OK SaveOneUser %v", user.Username))
				c.Redirect(302, "/admin/users")
				return
			}
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("Error ShouldBind %v", errA))
		}
	}
	c.HTML(http.StatusOK, "users/update.html", gin.H{
		//"messages": flashes(c),
		"user":     user,
		"username": helpers.Admin_GetUserName(c),
	})
}

func All_Users_Get(c *gin.Context) {
	result, err := svc.GetAllUsersWithTerrain()
	if err != nil {
		log.Printf("GetAllUsersWithTerrain err:[%v]\n", err)
		c.Redirect(http.StatusFound, "/admin/accueil/")
		return
	} else {
		log.Printf("GetAllUsersWithTerrain OK result:[%v]\n", result)
		userLists, userCount, err := svc.GetAllUsers()
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error All_Users_Get %v", err))
		}
		c.HTML(http.StatusOK, "users/index.html", gin.H{
			"users":    userLists,
			"users2":   result,
			"nb":       userCount,
			"username": helpers.Admin_GetUserName(c),
		})

	}
}
