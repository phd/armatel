package controllers

import (
	"armatel/helpers"
	svc "armatel/services"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "gorm.io/driver/mysql"
)

func Client_AudioInFrame_Get(c *gin.Context) {
	id := c.Param("id")
	terrain := c.Param("terrain")
	fmt.Printf("AudioInFrame_Get id :[%v]\n", id)
	fmt.Printf("AudioInFrame_Get terrain:[%v]\n", terrain)

	notre_terain := helpers.Client_GetUserName(c)
	fmt.Printf("notre_terain:[%v]\n", notre_terain)

	if notre_terain != terrain {
		helpers.FlashMessage(c, fmt.Sprintf("Error No de terrain"))
		c.Redirect(http.StatusFound, "/client/home/")
		return
	}

	audio, wavdata, err := svc.GetOneAudio(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error AudioInFrame_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/client/home/")
		return
	}
	helpers.FlashMessage(c, fmt.Sprintf("OK AudioInFrame_Get '%d' audio %v", id, audio.ID))
	c.HTML(http.StatusOK, "audios/audioinframe.html", gin.H{
		"audio":           audio,
		"AudioDataBase64": wavdata,
		"username":        helpers.Client_GetUserName(c),
	})
}

func Admin_AudioInFrame_Get(c *gin.Context) {
	id := c.Param("id")
	terrain := c.Param("terrain")
	fmt.Printf("AudioInFrame_Get id :[%v]\n", id)
	fmt.Printf("AudioInFrame_Get terrain:[%v]\n", terrain)

	notre_terain := helpers.Client_GetUserName(c)
	fmt.Printf("notre_terain:[%v]\n", notre_terain)

	if notre_terain != terrain {
		helpers.FlashMessage(c, fmt.Sprintf("Error No de terrain"))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}

	audio, wavdata, err := svc.GetOneAudio(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error AudioInFrame_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}
	helpers.FlashMessage(c, fmt.Sprintf("OK AudioInFrame_Get '%d' audio %v", id, audio.ID))
	c.HTML(http.StatusOK, "audios/audioinframe.html", gin.H{
		"audio":           audio,
		"AudioDataBase64": wavdata,
		"username":        helpers.Admin_GetUserName(c),
	})
}

func Audio_Get(c *gin.Context) {
	id := c.Param("id")
	fmt.Printf("Audio_Get id Param:[%v]\n", id)
	audio, wavdata, err := svc.GetOneAudio(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error Audio_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}
	helpers.FlashMessage(c, fmt.Sprintf("OK Audio_Get '%d' audio %v", id, audio.ID))
	c.HTML(http.StatusOK, "audios/audio.html", gin.H{
		"audio":           audio,
		"AudioDataBase64": wavdata,
		//"username":        helpers.Admin_GetUserName(c),
		"username": helpers.Client_GetUserName(c),
	})
}

func AllAudios_Get(c *gin.Context) {
	fmt.Printf("AllAudios_Get\n")
	audios, nb, err := svc.GetAllAudios()
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error AllAudios_Get  error %v", err))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}
	helpers.FlashMessage(c, fmt.Sprintf("Au total %v audio(s)", nb))
	c.HTML(http.StatusOK, "audios/index.html", gin.H{
		"audios": audios,
		"nb":     nb,
		//"username": helpers.Admin_GetUserName(c),
		"username": helpers.Client_GetUserName(c),
	})
}
