// Unit Testing Controllers

package controllers

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	// SAMPLE func TestGetUserByID(t *testing.T) {
	// Setup
	router := gin.Default()
	// SAMPLE: router.GET("/users/:id", controllers.GetUserByID)
	router.GET("/login", Admin_LoginForm_GET)

	// Create a test request
	// SAMPLE: req, err := http.NewRequest("GET", "/users/123", nil)
	req, err := http.NewRequest("GET", "/login", nil)
	assert.NoError(t, err)

	// Perform the request
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// Assert the response
	assert.Equal(t, http.StatusOK, rec.Code)
	assert.Equal(t, "{\"message\":\"User found\"}", rec.Body.String())
}
