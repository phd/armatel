package controllers

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	//"gorm.io/driver/sqlite"

	"armatel/helpers"
	svc "armatel/services"

	_ "gorm.io/driver/mysql"
)

func Terrain_viewlogo1_Get(c *gin.Context) {
	id_logo := c.Param("id")
	fmt.Printf("Terrain_viewlogo1_Get id:[%v]\n", id_logo)

	idlogo, _ := strconv.Atoi(id_logo)
	if idlogo > 0 {

		var imgByte []uint8
		imgByte, err := svc.LogoFromBlob(id_logo)
		if err != nil {
			log.Println(err)
			helpers.FlashMessage(c, fmt.Sprintf("Error1 retrieving logo terrain '%v' error %v", id_logo, err))
			//c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		c.HTML(http.StatusOK, "terrains/viewlogo1.html", gin.H{
			//"messages":        flashes(c),
			"ID":              id_logo,
			"ImageDataBase64": base64.StdEncoding.EncodeToString(imgByte),
			"username":        helpers.Admin_GetUserName(c),
		})
	}

}

func Terrain_viewlogo2_Get(c *gin.Context) {

	id_logo := c.Param("id")
	fmt.Printf("Terrain_viewlogo2_Get id:[%v]\n", id_logo)

	idlogo, _ := strconv.Atoi(id_logo)
	if idlogo > 0 {
		var imgByte []uint8
		imgByte, err := svc.LogoFromBlob(id_logo)
		if err != nil {
			log.Println(err)
			//flashMessage(c, fmt.Sprintf("Error retrieving logo terrain '%v' error %v", id_logo, err))
			//c.Redirect(http.StatusFound, "/admin/terrains/")
			yourHtmlString := "<html><body>*</body></html>"
			c.Writer.WriteHeader(http.StatusOK)
			c.Writer.Write([]byte(yourHtmlString))
			return
		} else {
			c.HTML(http.StatusOK, "terrains/viewlogo2.html", gin.H{
				//"messages":        flashes(c),
				"ID":              id_logo,
				"ImageDataBase64": base64.StdEncoding.EncodeToString(imgByte),
				"username":        helpers.Admin_GetUserName(c),
			})
		}
	}

}

func unusedTerrain_viewlogo_Get(c *gin.Context) {
	id_terrain := c.Param("id")
	fmt.Printf("Terrain_viewlogo_Get id:[%v]\n", id_terrain)

	var imgByte []uint8
	//imgByte, err = mysqldb.LogoFromDB(db, id_terrain)
	imgByte, err := svc.ImgFromBlob(id_terrain, "picture")
	if err != nil {
		log.Println(err)
		helpers.FlashMessage(c, fmt.Sprintf("Error2 retrieving logo terrain '%v' error %v", id_terrain, err))
		//c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}
	c.HTML(http.StatusOK, "terrains/viewlogo.html", gin.H{
		//helpers.flashmessage,
		"ID": id_terrain,
		//"images":          images,
		"ImageDataBase64": base64.StdEncoding.EncodeToString(imgByte),
		"username":        helpers.Admin_GetUserName(c),
	})

}
