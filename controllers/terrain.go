package controllers

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	//"gorm.io/driver/sqlite"
	mysqldb "armatel/db"
	"armatel/entity"
	models "armatel/entity"
	"armatel/helpers"
	svc "armatel/services"

	_ "gorm.io/driver/mysql"
)

func Terrain_Get(c *gin.Context) {
	c.HTML(http.StatusOK, "terrains/create.html", gin.H{
		//helpers.flashmessage,
		"username": helpers.Admin_GetUserName(c),
	})
}

func Terrain_delete_Get(c *gin.Context) {
	if db, err := mysqldb.GetInstance(); err == nil {
		id := c.Param("id")
		fmt.Printf("terrain_delete_Get id:[%v]\n", id)

		_, err = db.NamedExec("delete from terrains where id=:id",
			map[string]interface{}{
				"id": id,
			})
		if err != nil {
			log.Println(err)
			helpers.FlashMessage(c, fmt.Sprintf("Error deleting terrain '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		helpers.FlashMessage(c, fmt.Sprintf("deleting terrain '%v' successfully.", id))
	}
	c.Redirect(http.StatusFound, "/admin/terrains/")
}

func Terrain_champs_Get(c *gin.Context) { //<td><a href="/terrains/view/{{.ID}}">{{ .ID }}</a></td>
	// ask service
	if true {
		id := c.Param("id")
		fmt.Printf("terrain_view_Get id Param:[%v]\n", id)
		var terr models.TerrainEntity
		terr, err := svc.GetOneTerrain(id)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		c.HTML(http.StatusOK, "terrains/champs.html", gin.H{
			//helpers.flashmessage,
			"ter":      terr,
			"username": helpers.Admin_GetUserName(c),
		})
	}
}

func Terrain_view_Get(c *gin.Context) { //<td><a href="/terrains/view/{{.ID}}">{{ .ID }}</a></td>
	// ask service
	if true {
		id := c.Param("id")
		fmt.Printf("terrain_view_Get id Param:[%v]\n", id)
		var terr models.TerrainEntity
		terr, err := svc.GetOneTerrain(id)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		c.HTML(http.StatusOK, "terrains/view.html", gin.H{
			//helpers.flashmessage,
			"ter":      terr,
			"username": helpers.Admin_GetUserName(c),
		})
	}
}

func Terrain_update_Post(c *gin.Context) {
	if true {
		id := c.PostForm("id")
		fmt.Printf("Terrain_update_Post id Param:[%v]\n", id)
		datefin := c.PostForm("DateFin")
		fmt.Printf("Terrain_update_Post datefin:[%v]\n", datefin)

		//var terr entity.TerrainEntity
		terr, err := svc.GetOneTerrain(id)
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("Error GetOneTerrain '%v' error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return

		}
		if errA := c.ShouldBind(&terr); errA == nil {
			//c.String(http.StatusOK, `the body should be formA`)
			// Always an error is occurred by this because c.Request.Body is EOF now.
			//terr.ID = c.PostForm("id")
			date_fin := c.PostForm("date_fin") //			dateString := "2021-11-22"
			date_debut := c.PostForm("date_debut")
			fmt.Printf("Terrain_update_Post id date_fin:[%v]\n", date_fin)
			fmt.Printf("Terrain_update_Post id date_debut:[%v]\n", date_debut)

			DateFin, error1 := time.Parse("2006-01-02", date_fin)
			if error1 == nil {
				terr.DateFin = DateFin //time.Date(date_fin_arr[0], date_fin_arr[1], date_fin_arr[2], 0, 0, 0, t.Nanosecond(), t.Location())

				fmt.Printf("Terrain_update_Post id terr.DateFin:[%v]\n", terr.DateFin)
			} else {
				helpers.FlashMessage(c, fmt.Sprintf("Error date %v", error1))
			}
			/*DateDebut, error2 := time.Parse("2006-01-02", date_debut)
			if error2 == nil {
				terr.DateDebut = DateDebut
				fmt.Printf("Terrain_update_Post id terr.DateDebut:[%v]\n", terr.DateDebut)
			} else {
				helpers.FlashMessage(c, fmt.Sprintf("Error date %v", error2))
			}*/

			err := svc.SaveOneTerrain(terr)
			if err != nil {
				log.Println(err)
				helpers.FlashMessage(c, fmt.Sprintf("Error saving terrain '%v' error %v", id, err))
				c.Redirect(http.StatusFound, "/admin/terrains/")
				return
			}
			helpers.FlashMessage(c, fmt.Sprintf("OK saving terrain '%v' %v", id, date_fin))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			//c.Redirect(http.StatusFound, fmt.Sprintf("/admin/terrains/view/%v", id))
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("NOK saving terrain '%v' %v", id, errA))
			c.Redirect(http.StatusFound, "/admin/terrains/")
		}
	}
}

func Terrain_create_Post(c *gin.Context) {
	if true {
		var terr entity.TerrainEntity
		if errA := c.ShouldBind(&terr); errA == nil {
			//c.String(http.StatusOK, `the body should be formA`)
			// Always an error is occurred by this because c.Request.Body is EOF now.
			//terr.ID = c.PostForm("id")
			id, _, err := svc.CreateOneTerrain(terr)
			if err != nil {
				log.Println(err)
				helpers.FlashMessage(c, fmt.Sprintf("Création terrain '%v' error %v", id, err))
				c.Redirect(http.StatusFound, "/admin/terrains/")
				return
			}
			helpers.FlashMessage(c, fmt.Sprintf("OK saving terrain '%v' %v", id, terr))
			c.Redirect(http.StatusFound, fmt.Sprintf("/admin/terrains/view/%v", id))
			//c.Redirect(http.StatusFound, fmt.Sprintf("/admin/terrains/view/%v", id))
		} else {
			helpers.FlashMessage(c, fmt.Sprintf("ShouldBind error %v", errA))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
	}
}

func Terrains_Get(c *gin.Context) {
	fmt.Printf("Terrains_Get %v\n", 1)
	if true {
		var terrLists []models.TerrainEntity
		terrLists, terrCount, err := svc.GetAllTerrains()
		if err != nil {
			helpers.FlashMessage(c, fmt.Sprintf("GetAllTerrains error %v", err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		c.HTML(http.StatusOK, "terrains/index.html", gin.H{
			"terrains": terrLists,
			"count":    terrCount,
			//helpers.flashmessage,
			"success":  helpers.GetFlashCookie(c, "success"),
			"username": helpers.Admin_GetUserName(c),
			/*"no_page":        no_page,
			"no_page_prev":   no_page_prev,
			"no_page_next":   no_page_next,
			"items_per_page": items_per_page,*/
		})

	}
}
