package controllers

import (
	mysqldb "armatel/db"
	"armatel/helpers"
	svc "armatel/services"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	//"gorm.io/driver/sqlite"

	_ "gorm.io/driver/mysql"
)

func Terrain_view_pwd_Get(c *gin.Context) { //<td><a href="/terrains/view/{{.ID}}">{{ .ID }}</a></td>
	id := c.Param("id")
	fmt.Printf("Terrain_view_pwd_Get id:[%v]\n", id)
	terr, err := svc.GetOneTerrain(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/admin/terrains/")
		return
	}
	c.HTML(http.StatusOK, "terrains/passwd.html", gin.H{
		//helpers.flashmessage,
		"id": terr.ID,
	})
}

func Terrain_updatepwd_Post(c *gin.Context) {
	if db, err := mysqldb.GetInstance(); err == nil {
		id := c.PostForm("id")
		newpwd := c.PostForm("newpwd")
		fmt.Println("The Host: ", c.Request.Host)
		fmt.Println("The URL.Path: ", c.Request.URL.Path)
		if len(newpwd) < 3 {
			log.Println(err)
			helpers.FlashMessage(c, fmt.Sprintf("Error mot de passe de longueur %d trop court ", len(newpwd)))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			//c.Redirect(http.StatusFound, fmt.Sprintf("/admin/terrains/passwd/%s", id))
			return
		}

		h := sha1.New()
		h.Write([]byte(newpwd))
		sha1_hash := hex.EncodeToString(h.Sum(nil))
		fmt.Printf("sha1_hash:[%v]\n", sha1_hash)

		_, err = db.NamedExec("update terrains set cryptpwd=:pwd_param where id=:id",
			map[string]interface{}{
				"id":        id,
				"pwd_param": sha1_hash,
			})
		if err != nil {
			log.Println(err)
			helpers.FlashMessage(c, fmt.Sprintf("Error MAJ mot de passe terrain %v error %v", id, err))
			c.Redirect(http.StatusFound, "/admin/terrains/")
			return
		}
		helpers.FlashMessage(c, fmt.Sprintf("MAJ OK du mot de passe terrain %v successfully.", id))
		c.Redirect(http.StatusFound, "/admin/terrains/")
	}
}
