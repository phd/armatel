package controllers

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"armatel/entity"
	models "armatel/entity"
	"armatel/helpers"
	svc "armatel/services"

	log "github.com/sirupsen/logrus"

	_ "gorm.io/driver/mysql"
)

type AvancementItem struct {
	id            int           `db:"id"`
	id_terrain    sql.NullInt64 `db:"id_terrain"`
	date_creation sql.NullTime  `db:"date_creation"`
	realises      sql.NullInt64 `db:"realises"`
}

func Client_Accueil_Get(c *gin.Context) {
	c.HTML(http.StatusOK, "accueil.html", gin.H{
		//"messages": flashes(c),
		"username": helpers.Client_GetUserName(c),
	})

}

func Admin_Accueil_Get(c *gin.Context) {
	c.HTML(http.StatusOK, "accueil.html", gin.H{
		//"messages": flashes(c),
		"username": helpers.Admin_GetUserName(c),
	})
}

func Client_Terrain_view_Get(c *gin.Context) { //<td><a href="/client/view/{{.ID}}">{{ .ID }}</a></td>
	id := c.Param("id")
	log.Printf("Terrain_ecoutes_view_Get id Param:[%v]\n", id)
	var ecoutes []entity.EcouteEntity
	ecoutes, nb, err := svc.GetEcoutesOfTerrain(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/client/home/")
		return
	}
	var terr models.TerrainEntity
	terr, err = svc.GetOneTerrain(id)
	if err != nil {
		helpers.FlashMessage(c, fmt.Sprintf("Error terrain_view_Get '%d' error %v", id, err))
		c.Redirect(http.StatusFound, "/client/home/")
		return
	}
	progress := terr.Realises * 100 / terr.Objectif
	todo := 100 - progress

	DDMMYYYY := "02/01/2006" //YYYYMMDD := "2006-01-02"
	c.HTML(http.StatusOK, "client/index.html", gin.H{
		"maintenant": time.Now().Format(DDMMYYYY),
		//"messages":    flashes(c),
		"ecoutes":     ecoutes,
		"terrain":     terr,
		"idTerrain":   id,
		"nb":          nb,
		"pourcentage": progress,
		"a_faire":     todo,
		"username":    helpers.Client_GetUserName(c),
	})
}
